package com.gintelsys.partners.adaptes;

public class CommentItem {
    public String cid, pid, nic, fec, men;

    public CommentItem(String cid, String pid, String nic, String fec, String men){
        this.cid = cid;
        this.pid = pid;
        this.nic = nic;
        this.fec = fec;
        this.men = men;
    }
}
