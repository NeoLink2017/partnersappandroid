package com.gintelsys.partners

import android.app.Activity
import android.content.ClipData
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.gintelsys.partners.adaptes.CommentItem
import com.gintelsys.partners.adaptes.PostAdapter
import com.gintelsys.partners.adaptes.PostItem
import com.gintelsys.partners.adaptes.ReactionItem
import com.gintelsys.partners.data.dbHandler
import com.gintelsys.partners.network.OkHttpRequest
import com.gintelsys.partners.utils.GeneralUtils
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_home.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.IOException

class HomeActivity : AppCompatActivity() {
    private lateinit var adapter: PostAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var gu : GeneralUtils
    private lateinit var dba : dbHandler
    private lateinit var dbposts : Cursor;
    lateinit var items: ArrayList<PostItem>
    lateinit var comments: ArrayList<CommentItem>
    lateinit var reactions: ArrayList<ReactionItem>
    lateinit var item: PostItem
    lateinit var comment: CommentItem
    lateinit var reaction: ReactionItem
    var s_res = ""
    var s_men = ""
    var s_est = ""
    var client = OkHttpClient()
    var request = OkHttpRequest(client)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        gu = GeneralUtils(this)
        gu.setRestServer("production")
        dba = dbHandler(this, null, null, 1)
        FirebaseMessaging.getInstance().subscribeToTopic("prtns_common")

        editText3.setFilters(arrayOf<InputFilter>(InputFilter.AllCaps()))

        swiperefresh.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            swiperefresh.isRefreshing = true
            loadAbove(dba.getParamByName("unidevid"), dba.maxPID)
        })
        imageView10.setOnClickListener(View.OnClickListener {
            goToPublish()
        })
        imageView11.setOnClickListener(View.OnClickListener {
            goToBoleta()
        })
        imageView14.setOnClickListener(View.OnClickListener {
            goToSettings()
        })
        editText3.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //Log.i("VIEWDATA", p0.toString())
                if(p0.toString().length > 0){
                    searchInPosts(p0.toString(), "50")
                } else {
                    loadPostFromDB()
                }
            }
        })

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this) {
            instanceIdResult -> val deviceToken = instanceIdResult.token
            //Log.i("VIEWDATA", deviceToken)
            val oldfbid = dba.getParamByName("fbid")
            if(!(oldfbid.equals(deviceToken))){
                dba.updateParamByName("fbid", deviceToken)
                gu.sendFBID(deviceToken)
            }
        }

        initLayout()

        if(dba.hasPosts()){
            items.clear()
            rv_posts.visibility = View.GONE
            ll_progress.visibility = View.VISIBLE
            dbposts = dba.pullPost("50")
            while(dbposts.moveToNext()){
                item = PostItem(
                    dbposts.getString(1),
                    dbposts.getString(2),
                    dbposts.getString(3),
                    dbposts.getString(4),
                    dbposts.getString(5),
                    dbposts.getString(6),
                    dbposts.getString(7),
                    dbposts.getString(8),
                    dbposts.getString(9),
                    dbposts.getString(10),
                    dbposts.getString(11),
                    dbposts.getString(12),
                    dbposts.getString(13),
                    dbposts.getString(14),
                    dbposts.getString(15),
                    dbposts.getString(16),
                    "dba",
                    dbposts.getString(18),
                    dbposts.getString(19),
                    dbposts.getString(20))
                items.add(item)

            }
            adapter.notifyDataSetChanged()
            ll_progress.visibility = View.GONE
            rv_posts.visibility = View.VISIBLE
        } else {
            if(gu.isNetworkAvailable){
                callFirstLoad()
            } else {
                gu.displayAlert("SIN ACCESO A INTERNET", "Partners App requiere acceso a Internet. Verifica tu conexion o revisa si esta App tiene Permisos para Internet", this)
            }
        }
    }

    fun loadPostFromDB(){
        rv_posts.visibility = View.GONE
        ll_progress.visibility = View.VISIBLE
        items.clear()
        dbposts = dba.pullPost("50");
        while(dbposts.moveToNext()){
            item = PostItem(
                dbposts.getString(1),
                dbposts.getString(2),
                dbposts.getString(3),
                dbposts.getString(4),
                dbposts.getString(5),
                dbposts.getString(6),
                dbposts.getString(7),
                dbposts.getString(8),
                dbposts.getString(9),
                dbposts.getString(10),
                dbposts.getString(11),
                dbposts.getString(12),
                dbposts.getString(13),
                dbposts.getString(14),
                dbposts.getString(15),
                dbposts.getString(16),
                "dba",
                dbposts.getString(18),
                dbposts.getString(19),
                dbposts.getString(20))
            items.add(item)

        }
        adapter.notifyDataSetChanged()
        ll_progress.visibility = View.GONE
        rv_posts.visibility = View.VISIBLE
    }

    fun initLayout() {
        linearLayoutManager = LinearLayoutManager(this)
        rv_posts.layoutManager = linearLayoutManager
        items = ArrayList<PostItem>()
        comments = ArrayList<CommentItem>()
        reactions = ArrayList<ReactionItem>()
        adapter = PostAdapter(items, applicationContext)
        rv_posts.adapter = adapter
    }

    fun searchInPosts(filter: String, limit: String){
        rv_posts.visibility = View.GONE
        ll_progress.visibility = View.VISIBLE
        items.clear()
        dbposts = dba.searchPost(filter, limit)
        while(dbposts.moveToNext()){
            item = PostItem(
                dbposts.getString(1),
                dbposts.getString(2),
                dbposts.getString(3),
                dbposts.getString(4),
                dbposts.getString(5),
                dbposts.getString(6),
                dbposts.getString(7),
                dbposts.getString(8),
                dbposts.getString(9),
                dbposts.getString(10),
                dbposts.getString(11),
                dbposts.getString(12),
                dbposts.getString(13),
                dbposts.getString(14),
                dbposts.getString(15),
                dbposts.getString(16),
                "dba",
                dbposts.getString(18),
                dbposts.getString(19),
                dbposts.getString(20))
            items.add(item)

        }
        adapter.notifyDataSetChanged()
        ll_progress.visibility = View.GONE
        rv_posts.visibility = View.VISIBLE
    }

    private fun loadAbove(devid: String, pid: String) {
        val url = gu.getApiRestURL("loaabo")
        val vals: HashMap<String, String> = hashMapOf(
            "v1" to devid,
            "v2" to pid
        )

        request.POST(url, vals, object: Callback {
            override fun onResponse(call: Call?, response: Response) {
                val responseData = response.body()?.string()
                runOnUiThread{
                    try {
                        var json = JSONObject(responseData)
                        var jsdats = json.getJSONArray("dats")
                        var jsdtArrayObject = jsdats.getJSONObject(0)

                        s_res = jsdtArrayObject.getString("res")
                        s_men = jsdtArrayObject.getString("men")
                        s_est = jsdtArrayObject.getString("est")

                        if(s_est.equals("FAIL")){
                            dba.removeAccount()
                            finish()
                        } else {
                            if(s_men.equals("OK")){
                                var jsdatarray = jsdtArrayObject.getJSONArray("dtarray")
                                for(i in 0..jsdatarray!!.length() - 1){
                                    item = PostItem(jsdatarray.getJSONObject(i).getString("pid"),
                                        jsdatarray.getJSONObject(i).getString("typ"),
                                        jsdatarray.getJSONObject(i).getString("nic"),
                                        jsdatarray.getJSONObject(i).getString("ava"),
                                        jsdatarray.getJSONObject(i).getString("fec"),
                                        jsdatarray.getJSONObject(i).getString("ori"),
                                        jsdatarray.getJSONObject(i).getString("him"),
                                        jsdatarray.getJSONObject(i).getString("tit"),
                                        jsdatarray.getJSONObject(i).getString("img"),
                                        jsdatarray.getJSONObject(i).getString("men"),
                                        jsdatarray.getJSONObject(i).getString("tco"),
                                        jsdatarray.getJSONObject(i).getString("tli"),
                                        jsdatarray.getJSONObject(i).getString("tme"),
                                        jsdatarray.getJSONObject(i).getString("tms"),
                                        jsdatarray.getJSONObject(i).getString("tmm"),
                                        jsdatarray.getJSONObject(i).getString("idr"),
                                        "srv",
                                        jsdatarray.getJSONObject(i).getString("rea"),
                                        jsdatarray.getJSONObject(i).getString("sad"),
                                        jsdatarray.getJSONObject(i).getString("lol")

                                    )
                                    items.add(0, item)
                                    dba.addPostToLocalDB(item.pid,
                                        item.typ,
                                        item.usu,
                                        item.ava,
                                        item.fec,
                                        item.ori,
                                        item.him,
                                        item.tit,
                                        item.img,
                                        item.men,
                                        item.tco,
                                        item.tli,
                                        item.tme,
                                        item.tms,
                                        item.tmm,
                                        item.idr,
                                        item.sto,
                                        item.rea,
                                        item.tsa,
                                        item.tlo
                                    )
                                }
                                callLoadCommentsAbove()
                            }
                        }
                    } catch (e: JSONException) {
                        //Log.i("VIEWDATA",  e.message)
                    }
                }
            }
            override fun onFailure(call: Call?, e: IOException?) {
                //Log.i("VIEWDATA",  e.toString())
            }
        })
    }

    fun callSyncReactions(){
        val url = gu.getApiRestURL("synrea")
        val vals: HashMap<String, String> = hashMapOf(
            "v1" to dba.getParamByName("unidevid"),
            "v2" to dba.minPID,
            "v3" to dba.maxPID
        )

        request.POST(url, vals, object: Callback {
            override fun onResponse(call: Call?, response: Response) {
                val responseData = response.body()?.string()
                runOnUiThread{
                    try {
                        var json = JSONObject(responseData)
                        var jsdats = json.getJSONArray("dats")
                        var jsdtArrayObject = jsdats.getJSONObject(0)

                        s_res = jsdtArrayObject.getString("res")
                        s_men = jsdtArrayObject.getString("men")
                        if(s_men.equals("OK")){
                            var jsdatarray = jsdtArrayObject.getJSONArray("dtarray")
                            for(i in 0..jsdatarray!!.length() - 1){
                                reaction = ReactionItem(jsdatarray.getJSONObject(i).getString("pid"),
                                    jsdatarray.getJSONObject(i).getString("rea"),
                                    jsdatarray.getJSONObject(i).getString("nic"),
                                    jsdatarray.getJSONObject(i).getString("ava")
                                )
                                reactions.add(reaction)
                                dba.addReactionToLocalDB(reaction.pid, reaction.rea, reaction.nic, reaction.ava)
                            }
                            dba.syncReactions()
                            loadPostFromDB()
                            swiperefresh.isRefreshing = false
                        }
                    } catch (e: JSONException) {}
                }
            }
            override fun onFailure(call: Call?, e: IOException?) {}
        })
    }

    private fun callFirstLoad() {
        rv_posts.visibility = View.GONE
        ll_progress.visibility = View.VISIBLE

        val url = gu.getApiRestURL("firloa")
        val vals: HashMap<String, String> = hashMapOf(
            "v1" to dba.getParamByName("unidevid")
        )

        request.POST(url, vals, object: Callback {
            override fun onResponse(call: Call?, response: Response) {
                val responseData = response.body()?.string()
                runOnUiThread{
                    try {
                        var json = JSONObject(responseData)
                        var jsdats = json.getJSONArray("dats")
                        var jsdtArrayObject = jsdats.getJSONObject(0)

                        s_res = jsdtArrayObject.getString("res")
                        s_men = jsdtArrayObject.getString("men")

                        if(s_men.equals("OK")){
                            var jsdatarray = jsdtArrayObject.getJSONArray("dtarray")
                            for(i in 0..jsdatarray!!.length() - 1){
                                item = PostItem(jsdatarray.getJSONObject(i).getString("pid"),
                                    jsdatarray.getJSONObject(i).getString("typ"),
                                    jsdatarray.getJSONObject(i).getString("nic"),
                                    jsdatarray.getJSONObject(i).getString("ava"),
                                    jsdatarray.getJSONObject(i).getString("fec"),
                                    jsdatarray.getJSONObject(i).getString("ori"),
                                    jsdatarray.getJSONObject(i).getString("him"),
                                    jsdatarray.getJSONObject(i).getString("tit"),
                                    jsdatarray.getJSONObject(i).getString("img"),
                                    jsdatarray.getJSONObject(i).getString("men"),
                                    jsdatarray.getJSONObject(i).getString("tco"),
                                    jsdatarray.getJSONObject(i).getString("tli"),
                                    jsdatarray.getJSONObject(i).getString("tme"),
                                    jsdatarray.getJSONObject(i).getString("tms"),
                                    jsdatarray.getJSONObject(i).getString("tmm"),
                                    jsdatarray.getJSONObject(i).getString("idr"),
                                    "srv",
                                    jsdatarray.getJSONObject(i).getString("rea"),
                                    jsdatarray.getJSONObject(i).getString("sad"),
                                    jsdatarray.getJSONObject(i).getString("lol")
                                )
                                dba.addPostToLocalDB(item.pid,
                                    item.typ,
                                    item.usu,
                                    item.ava,
                                    item.fec,
                                    item.ori,
                                    item.him,
                                    item.tit,
                                    item.img,
                                    item.men,
                                    item.tco,
                                    item.tli,
                                    item.tme,
                                    item.tms,
                                    item.tmm,
                                    item.idr,
                                    item.sto,
                                    item.rea,
                                    item.tsa,
                                    item.tlo
                                )
                            }
                            callFirstCommentLoad()
                        } else {
                            ll_progress.visibility = View.GONE
                            rv_posts.visibility = View.VISIBLE
                        }
                    } catch (e: JSONException) {
                        //Log.i("VIEWDATA",  e.message)
                    }
                }
            }
            override fun onFailure(call: Call?, e: IOException?) {
                //Log.i("VIEWDATA",  e.toString())
            }
        })
    }

    fun callFirstCommentLoad() {
        val url = gu.getApiRestURL("fircomloa")
        val vals: HashMap<String, String> = hashMapOf(
            "v1" to dba.getParamByName("unidevid"),
            "v2" to dba.minPID,
            "v3" to dba.maxPID
        )

        request.POST(url, vals, object: Callback {
            override fun onResponse(call: Call?, response: Response) {
                val responseData = response.body()?.string()
                runOnUiThread{
                    try {
                        var json = JSONObject(responseData)
                        var jsdats = json.getJSONArray("dats")
                        var jsdtArrayObject = jsdats.getJSONObject(0)

                        s_res = jsdtArrayObject.getString("res")
                        s_men = jsdtArrayObject.getString("men")

                        if(s_men.equals("OK")){
                            var jsdatarray = jsdtArrayObject.getJSONArray("dtarray")
                            for(i in 0..jsdatarray!!.length() - 1){
                                comment = CommentItem(jsdatarray.getJSONObject(i).getString("cid"),
                                    jsdatarray.getJSONObject(i).getString("pid"),
                                    jsdatarray.getJSONObject(i).getString("nic"),
                                    jsdatarray.getJSONObject(i).getString("fec"),
                                    jsdatarray.getJSONObject(i).getString("men")
                                )
                                comments.add(comment)
                                dba.addCommentsToLocalDB(comment.cid, comment.pid, comment.nic, comment.fec, comment.men)
                            }
                            dba.syncPostAndComments()
                            //loadPostFromDB()
                            dba.clearLocalReactions()
                            callSyncReactions()
                        }
                    } catch (e: JSONException) {}
                }
            }
            override fun onFailure(call: Call?, e: IOException?) {}
        })
    }

    fun callLoadCommentsAbove() {
        val url = gu.getApiRestURL("loacomabo")
        val vals: HashMap<String, String> = hashMapOf(
            "v1" to dba.getParamByName("unidevid"),
            "v2" to dba.maxCID,
            "v3" to dba.minPID,
            "v4" to dba.maxPID
        )
        request.POST(url, vals, object: Callback {
            override fun onResponse(call: Call?, response: Response) {
                val responseData = response.body()?.string()
                runOnUiThread{
                    try {
                        var json = JSONObject(responseData)
                        var jsdats = json.getJSONArray("dats")
                        var jsdtArrayObject = jsdats.getJSONObject(0)

                        s_res = jsdtArrayObject.getString("res")
                        s_men = jsdtArrayObject.getString("men")

                        if(s_men.equals("OK")){
                            var jsdatarray = jsdtArrayObject.getJSONArray("dtarray")
                            for(i in 0..jsdatarray!!.length() - 1){
                                comment = CommentItem(jsdatarray.getJSONObject(i).getString("cid"),
                                    jsdatarray.getJSONObject(i).getString("pid"),
                                    jsdatarray.getJSONObject(i).getString("nic"),
                                    jsdatarray.getJSONObject(i).getString("fec"),
                                    jsdatarray.getJSONObject(i).getString("men")
                                )
                                comments.add(comment)
                                dba.addCommentsToLocalDB(comment.cid, comment.pid, comment.nic, comment.fec, comment.men)
                            }
                            dba.syncPostAndComments()
                            dba.clearLocalReactions()
                            callSyncReactions()
                        }
                    } catch (e: JSONException) {}
                }
            }
            override fun onFailure(call: Call?, e: IOException?) {}
        })
    }

    fun goToBoleta() {
        val i = Intent(applicationContext, BoletaActivity::class.java)
        startActivity(i)
    }

    fun goToPublish() {
        val i = Intent(applicationContext, PublishActivity::class.java)
        startActivity(i)
    }

    fun goToSettings() {
        val i = Intent(applicationContext, SettingsActivity::class.java)
        startActivity(i)
    }

    override fun onResume() {
        super.onResume()

        swiperefresh.isRefreshing = true
        loadAbove(dba.getParamByName("unidevid"), dba.maxPID)
    }
}