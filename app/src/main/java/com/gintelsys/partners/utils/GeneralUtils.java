package com.gintelsys.partners.utils;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import com.gintelsys.partners.data.dbHandler;
import okhttp3.*;

import java.io.*;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class GeneralUtils {
    static Context mContext;
    private String apiurl = "";
    OkHttpClient client = new OkHttpClient();
    dbHandler dba;

    public GeneralUtils(Context mContext){
        this.mContext = mContext;
        dba = new dbHandler(mContext, null, null, 1);
    }

    public void setContext(Context mContext){
        this.mContext = mContext;
    }

    public void setRestServer(String prefix){
        switch (prefix) {
            case "production":
                apiurl = "https://imperium.pnc.gob.sv/apis/prtnrs/";
                break;
            default:
                break;
        }
    }

    private String setUniqueURL(){
        return "&m=" + String.valueOf(SystemClock.currentThreadTimeMillis());
    }

    public String getApiRestURL(String requested){
        return apiurl + "?dp=" + requested + setUniqueURL();
    }

    public void displayAlert(String tit, String mes, Context context){
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(tit);
        alertDialogBuilder
                .setMessage(mes)
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                    }
                });
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public boolean isNetworkAvailable() {
        //if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            Log.i("VIEWDATA", "Per OK");
            ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        //} else {
        //    Log.i("VIEWDATA", "Per NOT OK");
        //    return false;
        //}
    }

    public String getIPAddress(boolean useIPv4) {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_WIFI_STATE) != PackageManager.PERMISSION_GRANTED) {
            try {
                List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
                for (NetworkInterface intf : interfaces) {
                    List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                    for (InetAddress addr : addrs) {
                        if (!addr.isLoopbackAddress()) {
                            String sAddr = addr.getHostAddress();
                            //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                            boolean isIPv4 = sAddr.indexOf(':')<0;

                            if (useIPv4) {
                                if (isIPv4)
                                    return sAddr;
                            } else {
                                if (!isIPv4) {
                                    int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                    return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) { } // for now eat exceptions
            return "NOT_FOUND";
        } else {
            return "NOT_FOUND";
        }
    }

    public String getMACAddress(String interfaceName) {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_WIFI_STATE) != PackageManager.PERMISSION_GRANTED) {
            try {
                List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
                for (NetworkInterface intf : interfaces) {
                    if (interfaceName != null) {
                        if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                    }
                    byte[] mac = intf.getHardwareAddress();
                    if (mac==null) return "";
                    StringBuilder buf = new StringBuilder();
                    for (int idx=0; idx<mac.length; idx++)
                        buf.append(String.format("%02X:", mac[idx]));
                    if (buf.length()>0) buf.deleteCharAt(buf.length()-1);
                    return buf.toString();
                }
            } catch (Exception ex) { } // for now eat exceptions
            return "NOT_FOUND";
        } else {
            return "NOT_FOUND";
        }
    }

    public void reactToPost(final String pid, final String reaccion, String dui, String ip, String codper, String usu, String tar){
        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
            String serres = "";
            @Override
            protected String doInBackground(String... params) {
                String pv1 = params[0];
                String pv2 = params[1];
                String pv3 = params[2];
                String pv4 = params[3];
                String pv5 = params[4];
                String pv6 = params[5];
                String pv7 = params[6];
                try {
                    FormBody.Builder formBuilder = new FormBody.Builder().add("v1", pv1);
                    formBuilder.add("v2", pv2);
                    formBuilder.add("v3", pv3);
                    formBuilder.add("v4", pv4);
                    formBuilder.add("v5", pv5);
                    formBuilder.add("v6", pv6);
                    formBuilder.add("v7", pv7);
                    RequestBody formBody = formBuilder.build();
                    Request request = new Request.Builder()
                            .url("https://imperium.pnc.gob.sv/apis/prtnrs/?dp=reatopos")
                            .post(formBody)
                            .build();
                    try(Response response = client.newCall(request).execute()) {
                        serres = response.body().string();
                    } catch(Exception e){
                        serres = e.getMessage();
                    }
                } catch(Exception e) {
                    serres = "FAIL";
                }

                return serres;
            }
            @Override
            protected void onPreExecute() {}

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                dba.updateReaction(pid, reaccion);
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(pid, reaccion, dui, ip, codper, usu, tar);
    }

    public void commentPost(final String pid, final String men, String dui, String ip, String codper, String usu, String tar){
        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
            String serres = "";
            @Override
            protected String doInBackground(String... params) {
                String pv1 = params[0];
                String pv2 = params[1];
                String pv3 = params[2];
                String pv4 = params[3];
                String pv5 = params[4];
                String pv6 = params[5];
                String pv7 = params[6];
                //Log.i("VIEWDATA",pv1 + "|" + pv2 + "|" + pv3 + "|" + pv4 + "|" + pv5);
                try {
                    FormBody.Builder formBuilder = new FormBody.Builder().add("v1", pv1);
                    formBuilder.add("v2", pv2);
                    formBuilder.add("v3", pv3);
                    formBuilder.add("v4", pv4);
                    formBuilder.add("v5", pv5);
                    formBuilder.add("v6", pv6);
                    formBuilder.add("v7", pv7);
                    RequestBody formBody = formBuilder.build();
                    Request request = new Request.Builder()
                            .url("https://imperium.pnc.gob.sv/apis/prtnrs/?dp=sencom")
                            .post(formBody)
                            .build();
                    try(Response response = client.newCall(request).execute()) {
                        serres = response.body().string();
                    } catch(Exception e){
                        serres = e.getMessage();
                    }
                } catch(Exception e) {
                    serres = "FAIL";
                }

                return serres;
            }
            @Override
            protected void onPreExecute() {}

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                //dba.updateReaction(pid, reaccion);
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(pid, men, dui, ip, codper, usu, tar);
    }

    public String getEncodedBase64(String cod){
        String r = "";
        byte[] data;
        try {
            data = cod.getBytes("UTF-8");
            r = Base64.encodeToString(data, Base64.DEFAULT);
        } catch (Exception ex){
            r = "NODATARETURNED";
        }
        return r;
    }

    public void copyFiles(File src, File dst) throws IOException {
        try (InputStream in = new FileInputStream(src)) {
            try (OutputStream out = new FileOutputStream(dst)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
    }

    public String encode(byte[] d)
    {
        if (d == null) return null;
        byte data[] = new byte[d.length+2];
        System.arraycopy(d, 0, data, 0, d.length);
        byte dest[] = new byte[(data.length/3)*4];

        // 3-byte to 4-byte conversion
        for (int sidx = 0, didx=0; sidx < d.length; sidx += 3, didx += 4)
        {
            dest[didx]   = (byte) ((data[sidx] >>> 2) & 077);
            dest[didx+1] = (byte) ((data[sidx+1] >>> 4) & 017 |
                    (data[sidx] << 4) & 077);
            dest[didx+2] = (byte) ((data[sidx+2] >>> 6) & 003 |
                    (data[sidx+1] << 2) & 077);
            dest[didx+3] = (byte) (data[sidx+2] & 077);
        }

        // 0-63 to ascii printable conversion
        for (int idx = 0; idx <dest.length; idx++)
        {
            if (dest[idx] < 26)     dest[idx] = (byte)(dest[idx] + 'A');
            else if (dest[idx] < 52)  dest[idx] = (byte)(dest[idx] + 'a' - 26);
            else if (dest[idx] < 62)  dest[idx] = (byte)(dest[idx] + '0' - 52);
            else if (dest[idx] < 63)  dest[idx] = (byte)'+';
            else            dest[idx] = (byte)'/';
        }

        // add padding
        for (int idx = dest.length-1; idx > (d.length*4)/3; idx--)
        {
            dest[idx] = (byte)'=';
        }
        return new String(dest);
    }

    /**
     * Encode a String using Base64 using the default platform encoding
     **/
    /*public final static String encode(String s) {
        return encode(s.getBytes());
    }*/

    public Bitmap decodeFile(String pname) {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pname, o);
        final int REQUIRED_SIZE = 60;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeFile(pname, o2);

        return bitmap;
    }

    public String ImageToBase64(ImageView iv) {
        String r = "";
        String t = "";
        byte[] data;

        BitmapDrawable drawable = (BitmapDrawable)iv.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 50, baos);
        try{
            t = baos.toString("UTF-8");
            data = t.getBytes("UTF-8");
            r = Base64.encodeToString(data, Base64.DEFAULT);
        } catch (Exception e){
        } finally {
            if(baos != null)
                try {
                    baos.close();
                } catch (Exception e2){
                    r = "";
                }
        }

        return r;
    }

    public void sendFBID(String fbm){
        class SendPostReqAsyncTask extends AsyncTask<String, Void, String>{
            String serres = "";
            @Override
            protected String doInBackground(String... params) {
                String pv1 = dba.getParamByName("unidevid");
                String pv2 = params[0];
                try {
                    //Log.i("DEPTO", getRootURL("aaaab"));
                    FormBody.Builder formBuilder = new FormBody.Builder().add("v1", pv1);
                    formBuilder.add("v2", pv2);
                    //Log.i("DEPTO", "pv1:" + pv1);
                    RequestBody formBody = formBuilder.build();
                    Request request = new Request.Builder()
                            .url("https://imperium.pnc.gob.sv/apis/prtnrs/?dp=updfbi")
                            .post(formBody)
                            .build();

                    try(Response response = client.newCall(request).execute()) {
                        serres = response.body().string();
                        //Log.i("VIEWDATA", serres);
                    } catch(Exception e){
                        //Log.i("VIEWDATA", "1- " + e.getMessage());
                        serres = e.getMessage();
                    }
                } catch(Exception e) {
                    //Log.i("VIEWDATA", "2- " + e.getMessage());
                    serres = "FAIL";
                }

                return serres;
            }
            @Override
            protected void onPreExecute() {}

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(fbm);
    }

    public int getRamdom(int min, int max){
        int res = new Random().nextInt((max - min) + 1) + min;
        return res;
    }
}
