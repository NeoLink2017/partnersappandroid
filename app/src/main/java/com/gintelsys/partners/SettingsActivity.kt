package com.gintelsys.partners

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.gintelsys.partners.data.dbHandler
import com.gintelsys.partners.utils.GeneralUtils
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {
    val unicode = 0x000AB
    lateinit var dba: dbHandler
    private lateinit var gu : GeneralUtils
    val appcod = BuildConfig.VERSION_CODE
    val appver = BuildConfig.VERSION_NAME


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        dba = dbHandler(this, null, null, 1)
        gu = GeneralUtils(this)
        gu.setRestServer("production")

        val back: String = String(Character.toChars(unicode))
        textView41.text =  " " + back + " " + back + " | Regresar a Publicaciones:"
        textView43.text = "Version " + appver + " | Compilado " + appcod.toString()

        textView41.setOnClickListener(View.OnClickListener {
            finish()
        })

        imageView34.setOnClickListener(View.OnClickListener {
            Toast.makeText(applicationContext, "La opcion de Mi Perfil estara disponible Pronto!", Toast.LENGTH_LONG).show()
        })
        imageView36.setOnClickListener(View.OnClickListener {
            Toast.makeText(applicationContext, "La opcion de Notificaciones estara disponible Pronto!", Toast.LENGTH_LONG).show()
        })
        imageView38.setOnClickListener(View.OnClickListener {
            Toast.makeText(applicationContext, "La opcion de Seguridad y Privacidad estara disponible Pronto!", Toast.LENGTH_LONG).show()
        })
        imageView40.setOnClickListener(View.OnClickListener {
            Toast.makeText(applicationContext, "La opcion de Acerca De estara disponible Pronto!", Toast.LENGTH_LONG).show()
        })
    }
}