package com.gintelsys.partners.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.Loader;
import android.util.Log;
import android.widget.Toast;
import com.gintelsys.partners.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class PdfDownloader {
    private static final int MEGABYTE = 1024 * 1024;

    public static String downloadFile(String fileUrl, File directory) {

        String downloadStatus;
        try {

            URL url = new URL(fileUrl);
            HttpURLConnection urlConnection =(HttpURLConnection) url.openConnection();
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(directory);
            int totalSize = urlConnection.getContentLength();

            Log.d("PDF", "Total size: " + totalSize);
            byte[] buffer = new byte[MEGABYTE];
            int bufferLength = 0;
            while((bufferLength = inputStream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, bufferLength);
            }
            downloadStatus = "success";
            fileOutputStream.close();
        }
        catch(FileNotFoundException e) {
            downloadStatus = "FileNotFoundException";
            e.printStackTrace();
        }
        catch(MalformedURLException e) {
            downloadStatus = "MalformedURLException";
            e.printStackTrace();
        }
        catch(IOException e) {
            downloadStatus = "IOException";
            e.printStackTrace();
        }
        Log.d("PDF", "Download Status: " + downloadStatus);
        return downloadStatus;
    }
}