package com.gintelsys.partners

import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.support.v4.content.FileProvider
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.View
import com.gintelsys.partners.data.dbHandler
import com.gintelsys.partners.utils.GeneralUtils
import com.gintelsys.partners.utils.PdfDownloader
import kotlinx.android.synthetic.main.activity_boleta.*
import java.io.File
import java.nio.file.Files
import java.nio.file.StandardCopyOption

class BoletaActivity : AppCompatActivity() {
    val unicode = 0x000AB
    lateinit var dba: dbHandler
    private lateinit var gu : GeneralUtils
    var dd = doDownload()
    var fd = forceDownload()
    var coden : String = ""
    var codeb = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_boleta)

        dba = dbHandler(this, null, null, 1)
        gu = GeneralUtils(this)
        gu.setRestServer("production")

        val oni = dba.getParamByName("oni")
        val did = dba.getParamByName("unidevid")
        val sid = dba.getParamByName("sid")
        coden = did + "&" + oni + "&" + sid
        codeb = gu.getEncodedBase64(coden)

        val fname = "boleta.pdf"
        var res = ""
        val wp = ContextWrapper(applicationContext)
        val file = wp.getDir("boletas", Context.MODE_PRIVATE)

        val back: String = String(Character.toChars(unicode))
        textView30.text =  " " + back + " " + back + " | Regresar a Publicaciones:"

        if(!file.exists()){
            file.mkdir()
        }
        val archivo = File(file, fname)
        if(archivo.exists()) {
            //Log.i("VIEWDATA", archivo.absolutePath)

            pdfView.fromFile(archivo)
                .defaultPage(1)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .load()
        } else {
            dd.setContext(applicationContext, codeb)
            dd.execute()


        }

        textView30.setOnClickListener(View.OnClickListener {
            finish()
        })
        srBoleta.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            srBoleta.isRefreshing = true
            reloadBoleta()
        })
        button3.setOnClickListener(View.OnClickListener {
            var docdir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            val copia = File(docdir, "boleta.pdf")
            if(copia.exists()){
                sendMail(copia)
            }
        })


    }

    class doDownload : AsyncTask<Void, Void, String>() {
        lateinit  var mContext : Context
        var code = ""

        fun setContext(mContext : Context, code : String){
            this.mContext = mContext
            this.code = code
        }

        override fun doInBackground(vararg params: Void?): String? {
            val fname = "boleta.pdf"
            var res = ""
            val wp = ContextWrapper(mContext)
            val file = wp.getDir("boletas", Context.MODE_PRIVATE)
            var docdir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            val copia = File(docdir, "boleta.pdf")
            var uri : String = Uri.parse("https://partners.pnc.gob.sv/apiRestPncBolMobInd/")
                .buildUpon()
                .appendQueryParameter("code", code)
                .build().toString()

            if(!file.exists()){
                file.mkdir()
            }

            val archivo = File(file, fname)
            if(!archivo.exists()) {
                res = PdfDownloader.downloadFile(uri.toString(), archivo)
                res = PdfDownloader.downloadFile(uri.toString(), copia)
            }
            return res
        }
        override fun onPreExecute() {
            super.onPreExecute()
        }
        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
        }
    }

    class forceDownload : AsyncTask<Void, Void, String>() {
        lateinit  var mContext : Context
        var code = ""

        fun setContext(mContext : Context, code : String){
            this.mContext = mContext
            this.code = code
        }

        override fun doInBackground(vararg params: Void?): String? {
            val fname = "boleta.pdf"
            var res = ""
            val wp = ContextWrapper(mContext)
            val file = wp.getDir("boletas", Context.MODE_PRIVATE)
            var docdir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            val copia = File(docdir, "boleta.pdf")
            var uri : String = Uri.parse("https://partners.pnc.gob.sv/apiRestPncBolMobInd/")
                .buildUpon()
                .appendQueryParameter("code", code)
                .build().toString()

            if(!file.exists()){
                file.mkdir()
            }
            val archivo = File(file, fname)
            res = PdfDownloader.downloadFile(uri.toString(), archivo)
            res = PdfDownloader.downloadFile(uri.toString(), copia)
            return res
        }
        override fun onPreExecute() {
            super.onPreExecute()
        }
        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
        }
    }

    fun reloadBoleta(){
        textView32.visibility = View.VISIBLE
        val wp = ContextWrapper(applicationContext)
        val file = wp.getDir("boletas", Context.MODE_PRIVATE)
        val fname = "boleta.pdf"
        var res = ""

        if(!file.exists()){
            file.mkdir()
        }
        val archivo = File(file, fname)
            fd.setContext(applicationContext, codeb)
            res = fd.execute().get()

            pdfView.fromFile(archivo)
                .defaultPage(1)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .load()
        textView32.visibility = View.GONE
        srBoleta.isRefreshing = false
    }

    fun sendMail(archivo: File){
        val fileURI = FileProvider.getUriForFile(this, "com.gintelsys.partners", archivo!!)
        val emailIntent = Intent(Intent.ACTION_SEND).apply {
            putExtra(Intent.EXTRA_SUBJECT, "Mi Boleta")
            putExtra(Intent.EXTRA_TEXT, "Probando")
            putExtra(Intent.EXTRA_STREAM, fileURI)
        }
        emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        emailIntent.type = "text/plain"
        startActivity(emailIntent)
    }
}