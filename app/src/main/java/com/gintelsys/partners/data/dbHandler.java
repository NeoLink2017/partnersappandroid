package com.gintelsys.partners.data;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import	android.content.Context;
import	android.content.ContentValues;
import	android.database.Cursor;
import android.util.Log;

public class dbHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 13;
    private static final  String DATABASE_NAME = "partnersDB.db";
    public static final String TABLE1_NAME = "CUENTA";
    public static final String TABLE2_NAME = "POSTS";
    public static final String TABLE3_NAME = "COMMENTS";
    public static final String TABLE4_NAME = "REACTIONS";

    public static final String TABLE1_COL1 = "_id";
    public static final String TABLE1_COL2 = "_nam";
    public static final String TABLE1_COL3 = "_val";

    public static final String TABLE2_COL1 = "_id";
    public static final String TABLE2_COL2 = "_pid"; //ID de Post
    public static final String TABLE2_COL3 = "_typ"; //Tipo: Post o Respuesta
    public static final String TABLE2_COL4 = "_usu"; //Usuario que publica
    public static final String TABLE2_COL5 = "_ava"; //Usuario que publica
    public static final String TABLE2_COL6 = "_fec"; //Fecha de post
    public static final String TABLE2_COL7 = "_ori"; //Origen APP o Web
    public static final String TABLE2_COL8 = "_him"; //Have Image
    public static final String TABLE2_COL9 = "_tit"; //Titulo del post
    public static final String TABLE2_COL10 = "_img"; //Imagen
    public static final String TABLE2_COL11 = "_men"; //Mensaje
    public static final String TABLE2_COL12 = "_tco"; //Total Comentarios
    public static final String TABLE2_COL13 = "_tli"; //Total Likes
    public static final String TABLE2_COL14 = "_tme"; //Total Me Encanta
    public static final String TABLE2_COL15 = "_tms"; //Total Me Sorprende
    public static final String TABLE2_COL16 = "_tmm"; //Total Me Molesta
    public static final String TABLE2_COL17 = "_idr"; //ID Respuesta
    public static final String TABLE2_COL18 = "_sto"; //ID Respuesta
    public static final String TABLE2_COL19 = "_rea"; //ID Respuesta
    public static final String TABLE2_COL20 = "_tsa"; //ID Respuesta
    public static final String TABLE2_COL21 = "_tso"; //ID Respuesta

    public static final String TABLE3_COL1 = "_id";
    public static final String TABLE3_COL2 = "_cid"; //ID de Commet
    public static final String TABLE3_COL3 = "_pid"; //ID de Post
    public static final String TABLE3_COL4 = "_usu"; //Usuario que publica
    public static final String TABLE3_COL5 = "_fec"; //Fecha de post
    public static final String TABLE3_COL6 = "_men"; //Mensaje

    public static final String TABLE4_COL1 = "_id";
    public static final String TABLE4_COL2 = "_pid"; //ID de Post
    public static final String TABLE4_COL3 = "_rea"; //ID de Reaccion
    public static final String TABLE4_COL4 = "_usu"; //Usuario que reaccion
    public static final String TABLE4_COL5 = "_ava"; //Avatar de Usuario

    public dbHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        String CREATE_TABLE1 = "CREATE TABLE " + TABLE1_NAME + "(" + TABLE1_COL1 + " INTEGER PRIMARY KEY, " + TABLE1_COL2 + " TEXT, " + TABLE1_COL3 + " TEXT)";
        String CREATE_TABLE2 = "CREATE TABLE " + TABLE2_NAME + "(" + TABLE2_COL1 + " INTEGER PRIMARY KEY, " + TABLE2_COL2 + " TEXT, " + TABLE2_COL3 + " TEXT, " + TABLE2_COL4 + " TEXT, " + TABLE2_COL5 + " TEXT, " + TABLE2_COL6 + " TEXT, " + TABLE2_COL7 + " TEXT, " + TABLE2_COL8 + " TEXT, " + TABLE2_COL9 + " TEXT, " + TABLE2_COL10 + " TEXT, " + TABLE2_COL11 + " TEXT, " + TABLE2_COL12 + " TEXT, " + TABLE2_COL13 + " TEXT, " + TABLE2_COL14 + " TEXT, " + TABLE2_COL15 + " TEXT, " + TABLE2_COL16 + " TEXT, " + TABLE2_COL17 + " TEXT, " + TABLE2_COL18 + " TEXT, " + TABLE2_COL19 + " TEXT, " + TABLE2_COL20 + " TEXT, " + TABLE2_COL21 + " TEXT)";
        String CREATE_TABLE3 = "CREATE TABLE " + TABLE3_NAME + "(" + TABLE3_COL1 + " INTEGER PRIMARY KEY, " + TABLE3_COL2 + " TEXT, " + TABLE3_COL3 + " TEXT, " + TABLE3_COL4 + " TEXT, " + TABLE3_COL5 + " TEXT, " + TABLE3_COL6 + " TEXT)";
        String CREATE_TABLE4 = "CREATE TABLE " + TABLE4_NAME + "(" + TABLE4_COL1 + " INTEGER PRIMARY KEY, " + TABLE4_COL2 + " TEXT, " + TABLE4_COL3 + " TEXT, " + TABLE4_COL4 + " TEXT, " + TABLE4_COL5 + " TEXT)";

        db.execSQL(CREATE_TABLE1);
        db.execSQL(CREATE_TABLE2);
        db.execSQL(CREATE_TABLE3);
        db.execSQL(CREATE_TABLE4);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE1_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE2_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE3_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE4_NAME);

        onCreate(db);
    }

    //GENERALES
    public int totRegsInTable(String table){
        String query = "SELECT * FROM " + table;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(query, null);
        int r = c.getCount();
        return r;
    }

    //CUENTA
    public void addCuentaItem(String nom, String val){
        ContentValues values = new ContentValues();
        values.put(TABLE1_COL2, nom);
        values.put(TABLE1_COL3, val);

        SQLiteDatabase db = this.getWritableDatabase();

        db.insert(TABLE1_NAME, null, values);
    }
    public boolean hasAccount(){
        boolean res;
        String query = "SELECT * FROM " + TABLE1_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery(query, null);
        if(r.getCount() > 0){
            res = true;
        } else {
            res = false;
        }

        return res;
    }
    public void removeAccount(){
        String query = "DELETE FROM " + TABLE1_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }
    public String getParamByName(String name){
        String r = "NOT_FOUND";
        String query = "SELECT " + TABLE1_COL3 + " FROM " + TABLE1_NAME + " WHERE " + TABLE1_COL2 + " = '" + name + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(query, null);
        if(c.getCount() == 1){
            c.moveToFirst();
            r = c.getString(0);
            c.close();
        }

        return r;
    }
    public void updateParamByName(String par, String val){
        String query = "UPDATE " + TABLE1_NAME + " SET " + TABLE1_COL3 + " = '" + val + "' WHERE " + TABLE1_COL2 + " = '" + par + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }

    //POSTS
    public void addPostToLocalDB(String pid, String typ, String usu, String ava, String fec, String ori, String him, String tit, String img, String men, String tco, String tli, String tme, String tms, String tmm, String idr, String sto, String rea, String tsa, String tlo){
        int t = checkIfPostExist(usu, men);
        if(t == 0){
            ContentValues values = new ContentValues();
            values.put(TABLE2_COL2, pid);
            values.put(TABLE2_COL3, typ);
            values.put(TABLE2_COL4, usu);
            values.put(TABLE2_COL5, ava);
            values.put(TABLE2_COL6, fec);
            values.put(TABLE2_COL7, ori);
            values.put(TABLE2_COL8, him);
            values.put(TABLE2_COL9, tit);
            values.put(TABLE2_COL10, img);
            values.put(TABLE2_COL11, men);
            values.put(TABLE2_COL12, tco);
            values.put(TABLE2_COL13, tli);
            values.put(TABLE2_COL14, tme);
            values.put(TABLE2_COL15, tms);
            values.put(TABLE2_COL16, tmm);
            values.put(TABLE2_COL17, idr);
            values.put(TABLE2_COL18, sto);
            values.put(TABLE2_COL19, rea);
            values.put(TABLE2_COL20, tsa);
            values.put(TABLE2_COL21, tlo);

            SQLiteDatabase db = this.getWritableDatabase();

            db.insert(TABLE2_NAME, null, values);
        }
    }
    public void deleteLocalPost(String pid){
        String query = "DELETE FROM " + TABLE2_NAME + " WHERE " + TABLE2_COL2 + " = '" + pid + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }
    private int checkIfPostExist(String usu, String men){
        int res;
        String query= "SELECT * FROM " + TABLE2_NAME + " WHERE " + TABLE2_COL4 + " = '" + usu + "' AND " + TABLE2_COL11 + " = '" + men + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery(query, null);
        res = r.getCount();
        r.close();
        return res;
    }
    public String getAvaFromPID(String pid){
        String r = "0";
        String query = "SELECT " + TABLE2_COL5 + " FROM " + TABLE2_NAME + " WHERE " + TABLE2_COL2 + " = '" + pid + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(query, null);
        if(c.getCount() == 1){
            c.moveToFirst();
            r = c.getString(0);
            c.close();
        }
        return r;
    }
    public boolean hasPosts(){
        boolean res;
        String query = "SELECT * FROM " + TABLE2_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery(query, null);
        if(r.getCount() > 0){
            res = true;
        } else {
            res = false;
        }

        return res;
    }
    public Cursor searchPost(String filter, String limit){
        String query = "SELECT * FROM " + TABLE2_NAME + " WHERE " + TABLE2_COL4 + " LIKE '%" + filter + "%' OR " + TABLE2_COL9 + " LIKE '%" + filter + "%' OR " + TABLE2_COL11 + " LIKE '%" + filter + "%' ORDER BY " + TABLE2_COL2 + " DESC LIMIT " + limit;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery(query, null);
        return r;
    }
    public Cursor pullPost(String limit) {
        String query = "SELECT * FROM " + TABLE2_NAME + " ORDER BY " + TABLE2_COL2 + " DESC LIMIT " + limit;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery(query, null);
        return r;
    }
    public String getMaxPID() {
        String r = "0";
        String query = "SELECT IFNULL(MAX(" + TABLE2_COL2 + "), 0) FROM " + TABLE2_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(query, null);
        if(c.getCount() == 1){
            c.moveToFirst();
            r = c.getString(0);
            c.close();
        }
        return r;
    }
    public String getMinPID() {
        String r = "0";
        String query = "SELECT IFNULL(MIN(" + TABLE2_COL2 + "), 0) FROM " + TABLE2_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(query, null);
        if(c.getCount() == 1){
            c.moveToFirst();
            r = c.getString(0);
            c.close();
        }
        return r;
    }
    public void updateReaction(String pid, String rea) {
        String query = "UPDATE " + TABLE2_NAME + " SET " + TABLE2_COL19 + " = '" + rea + "' WHERE " + TABLE2_COL2 + " = '" + pid + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }
    public void updateReactionCounter(String pid, String rea, String val) {
        String query = "";
        switch (rea) {
            case "1":
                query = "UPDATE " + TABLE2_NAME + " SET " + TABLE2_COL13 + " = '" + val + "' WHERE " + TABLE2_COL2 + " = '" + pid + "'";
                break;
            case "2":
                query = "UPDATE " + TABLE2_NAME + " SET " + TABLE2_COL14 + " = '" + val + "' WHERE " + TABLE2_COL2 + " = '" + pid + "'";
                break;
            case "3":
                query = "UPDATE " + TABLE2_NAME + " SET " + TABLE2_COL15 + " = '" + val + "' WHERE " + TABLE2_COL2 + " = '" + pid + "'";
                break;
            case "4":
                query = "UPDATE " + TABLE2_NAME + " SET " + TABLE2_COL16 + " = '" + val + "' WHERE " + TABLE2_COL2 + " = '" + pid + "'";
                break;
            case "5":
                query = "UPDATE " + TABLE2_NAME + " SET " + TABLE2_COL20 + " = '" + val + "' WHERE " + TABLE2_COL2 + " = '" + pid + "'";
                break;
            case "6":
                query = "UPDATE " + TABLE2_NAME + " SET " + TABLE2_COL21 + " = '" + val + "' WHERE " + TABLE2_COL2 + " = '" + pid + "'";
                break;
            default:
                break;
        }
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }
    public void clearAllLocalReactions(){
        String query = "UPDATE " + TABLE2_NAME + " SET " + TABLE2_COL13 + " = '0', " + TABLE2_COL14 + " = '0', " + TABLE2_COL15 + " = '0', " + TABLE2_COL16 + " = '0'";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }

    //COMMENTS
    public void addCommentsToLocalDB(String cid, String pid, String usu, String fec, String men){
        int c = countCommentsByCID(pid, men);
        if(c == 0){
            ContentValues values = new ContentValues();
            values.put(TABLE3_COL2, cid);
            values.put(TABLE3_COL3, pid);
            values.put(TABLE3_COL4, usu);
            values.put(TABLE3_COL5, fec);
            values.put(TABLE3_COL6, men);

            SQLiteDatabase db = this.getWritableDatabase();

            db.insert(TABLE3_NAME, null, values);
        }
    }
    private int countCommentsByCID(String pid, String com){
        int res;
        String query= "SELECT * FROM " + TABLE3_NAME + " WHERE " + TABLE3_COL3 + " = '" + pid + "' AND " + TABLE3_COL6 + " = '" + com + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery(query, null);
        res = r.getCount();
        r.close();

        return res;
    }
    public int countComments(String pid){
        int res;
        String query = "";
        if(pid.equals("0") || (pid.length() == 0)){
            query = "SELECT * FROM " + TABLE3_NAME;
        } else {
            query= "SELECT * FROM " + TABLE3_NAME + " WHERE " + TABLE3_COL3 + " = '" + pid + "'";
        }

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery(query, null);
        res = r.getCount();
        r.close();

        return res;
    }
    public Cursor commentsGroupedByPID(){
        String query = "SELECT " + TABLE3_COL3 + ", COUNT(1) FROM " + TABLE3_NAME + " GROUP BY " + TABLE3_COL3;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery(query, null);
        return r;
    }
    public void syncPostAndComments(){
        String query = "";
        Cursor c = commentsGroupedByPID();
        while(c.moveToNext()){
            query = "UPDATE " + TABLE2_NAME + " SET " + TABLE2_COL12 + " = '" + c.getString(1) + "' WHERE " + TABLE2_COL2 + " = '" + c.getString(0) + "'";
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(query);
        }
        c.close();
    }
    public String getMaxCID() {
        String r = "0";
        String query = "SELECT IFNULL(MAX(" + TABLE3_COL2 + "), 0) FROM " + TABLE3_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(query, null);
        if(c.getCount() == 1){
            c.moveToFirst();
            r = c.getString(0);
            c.close();
        }
        return r;
    }
    public Cursor pullCommentsByPID(String pid){
        String query = "SELECT " + TABLE3_COL4 + ", " + TABLE3_COL5 + ", " + TABLE3_COL6 + " FROM " + TABLE3_NAME + " WHERE " + TABLE3_COL3 + " = '" + pid + "' ORDER BY " + TABLE3_COL1 + " DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery(query, null);
        return r;
    }
    public Cursor pullReactionsByPID(String pid){
        String query = "SELECT " + TABLE4_COL3 + ", " + TABLE4_COL4 + ", " + TABLE4_COL5 + " FROM " + TABLE4_NAME + " WHERE " + TABLE4_COL2 + " = '" + pid + "' ORDER BY " + TABLE4_COL1 + " DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery(query, null);
        return r;
    }

    //REACTIONS
    public void addReactionToLocalDB(String pid, String rea, String usu, String ava){
        ContentValues values = new ContentValues();
        values.put(TABLE4_COL2, pid);
        values.put(TABLE4_COL3, rea);
        values.put(TABLE4_COL4, usu);
        values.put(TABLE4_COL5, ava);

        SQLiteDatabase db = this.getWritableDatabase();

        db.insert(TABLE4_NAME, null, values);
    }
    public void clearLocalReactions(){
        String query = "DELETE FROM " + TABLE4_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }
    public void syncReactions(){
        String query = "";
        clearAllLocalReactions();
        Cursor c = reactionsGroupedByPID();
        while(c.moveToNext()){
            switch (c.getString(1)){
                case "1":
                    query = "UPDATE " + TABLE2_NAME + " SET " + TABLE2_COL13 + " = '" + c.getString(2) + "' WHERE " + TABLE2_COL2 + " = '" + c.getString(0) + "'";
                    break;
                case "2":
                    query = "UPDATE " + TABLE2_NAME + " SET " + TABLE2_COL14 + " = '" + c.getString(2) + "' WHERE " + TABLE2_COL2 + " = '" + c.getString(0) + "'";
                    break;
                case "3":
                    query = "UPDATE " + TABLE2_NAME + " SET " + TABLE2_COL15 + " = '" + c.getString(2) + "' WHERE " + TABLE2_COL2 + " = '" + c.getString(0) + "'";
                    break;
                case "4":
                    query = "UPDATE " + TABLE2_NAME + " SET " + TABLE2_COL16 + " = '" + c.getString(2) + "' WHERE " + TABLE2_COL2 + " = '" + c.getString(0) + "'";
                    break;
                default:
                    break;
            }
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(query);
        }
        c.close();
    }
    public Cursor reactionsGroupedByPID(){
        String query = "SELECT " + TABLE4_COL2 + ", " + TABLE4_COL3 + ", COUNT(1) FROM " + TABLE4_NAME + " GROUP BY " + TABLE4_COL2 + ", " + TABLE4_COL3;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery(query, null);
        return r;
    }
}
