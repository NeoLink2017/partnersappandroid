package com.gintelsys.partners.adaptes;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.*;
import com.gintelsys.partners.R;
import com.gintelsys.partners.ShowReactionsActivity;
import com.gintelsys.partners.data.dbHandler;
import com.gintelsys.partners.utils.GeneralUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {
    private ArrayList<PostItem> postList = new ArrayList<>();
    PostItem post;
    OnItemClickListener mItemClickListener;
    ImageLoader il_pro, il_pos;
    GeneralUtils gu;
    Context mContext;
    dbHandler dba;
    Cursor coms;
    StringBuilder html;

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener){
        this.mItemClickListener = mItemClickListener;
    }

    public PostAdapter(ArrayList<PostItem> postList, Context context){
        gu = new GeneralUtils(context);
        gu.setRestServer("production");
        dba = new dbHandler(context, null, null, 1);

        this.mContext = context;
        this.postList = postList;
        il_pos = ImageLoader.getInstance();
        il_pro = ImageLoader.getInstance();
        il_pos.init(ImageLoaderConfiguration.createDefault(context));
        il_pro.init(ImageLoaderConfiguration.createDefault(context));
    }

    @Override
    public PostAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_post, viewGroup, false);

        return new PostAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PostAdapter.ViewHolder viewHolder, int i){
        String timg = "";
        String pimg = "";
        String iurl = "";
        String srea = "";
        post = postList.get(i);
        viewHolder.tv_usu.setText(post.usu);
        viewHolder.tv_pid.setText(post.pid);
        viewHolder.tv_ain.setText(post.rea);

            viewHolder.tv_prourl.setText(post.ava);
            pimg = viewHolder.tv_prourl.getText().toString().trim();
            if(pimg.equals("0") || pimg.length() == 0){
                viewHolder.iv_pos.setVisibility(View.GONE);
            } else {
                String iusu = viewHolder.tv_usu.getText().toString().trim() + ".png";
                ContextWrapper wp = new ContextWrapper(mContext);
                File file = wp.getDir("Images", Context.MODE_PRIVATE);
                File archivo =  new File(file, iusu);
                if(archivo.exists()){
                    String lsd = archivo.getPath();
                    viewHolder.iv_pro.setImageBitmap(BitmapFactory.decodeFile(lsd));
                } else {
                    if(pimg.length() > 40){
                        iurl = "https://imperium.pnc.gob.sv/apis/prtnrs/?dp=imahan&v1=" + pimg + "&v2=PRO";
                    } else {
                        iurl = "https://partners.pnc.gob.sv/mci2/avatar/" + pimg;
                    }
                    il_pro.displayImage(iurl, viewHolder.iv_pro, null, new ImageLoadingListener() {
                        @Override public void onLoadingStarted(String imageUri, View view) {}
                        @Override public void onLoadingFailed(String imageUri, View view, FailReason failReason) {}
                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            String iusu = viewHolder.tv_usu.getText().toString().trim() + ".png";
                            ContextWrapper wp = new ContextWrapper(mContext);
                            File file = wp.getDir("Images", Context.MODE_PRIVATE);
                            if(file.exists()){
                                File archivo =  new File(file, iusu);
                                if(!archivo.exists()) {
                                    FileOutputStream outputStream = null;
                                    try {
                                        outputStream = new FileOutputStream(archivo);
                                        loadedImage.compress(Bitmap.CompressFormat.PNG, 85, outputStream);
                                        outputStream.flush();
                                        outputStream.close();
                                    } catch (Exception ex){}
                                }
                            } else {
                                file.mkdir();
                                File archivo =  new File(file, iusu);
                                FileOutputStream outputStream = null;
                                try {
                                    outputStream = new FileOutputStream(archivo);
                                    loadedImage.compress(Bitmap.CompressFormat.PNG, 85, outputStream);
                                    outputStream.flush();
                                    outputStream.close();
                                } catch (Exception ex){}
                            }
                        }
                        @Override public void onLoadingCancelled(String imageUri, View view) {}
                    });
                }
            }

        if(post.typ.equals("PRI")){
            viewHolder.iv_lnk.setVisibility(View.GONE);
        } else {
            viewHolder.iv_lnk.setVisibility(View.VISIBLE);
            viewHolder.iv_tcm.setVisibility(View.GONE);
            viewHolder.tv_tcm.setVisibility(View.GONE);
        }
        switch (post.ori){
            case "AND":
                viewHolder.iv_ori.setImageResource(R.drawable.ori_android);
                break;
            case "WEB":
                viewHolder.iv_ori.setImageResource(R.drawable.ori_pc);
                break;
            default:
                viewHolder.iv_ori.setImageResource(R.drawable.ori_pc);
                break;
        }

        viewHolder.tv_fec.setText(post.fec);
        if(post.tit.equals("NO_TITLE")){
            viewHolder.tv_tit.setVisibility(View.GONE);
        } else {
            viewHolder.tv_tit.setVisibility(View.VISIBLE);
            viewHolder.tv_tit.setText(post.tit);
        }
        viewHolder.tv_pos.setText(post.men);
        viewHolder.tv_imgurl.setText(post.img);
        timg = viewHolder.tv_imgurl.getText().toString().trim();
        if(timg.equals("SINIMAGEN")){
            viewHolder.iv_pos.setVisibility(View.GONE);
        } else {
            String iusu = "p_" + viewHolder.tv_pid.getText().toString().trim() + ".png";
            ContextWrapper wp = new ContextWrapper(mContext);
            File file = wp.getDir("Images", Context.MODE_PRIVATE);
            File archivo =  new File(file, iusu);
            if(archivo.exists()){
                String lsd = archivo.getPath();
                viewHolder.iv_pos.setImageBitmap(BitmapFactory.decodeFile(lsd));
            } else {
                if(timg.length() > 40){
                    iurl = "https://imperium.pnc.gob.sv/apis/prtnrs/?dp=imahan&v1=" + timg + "&v2=APP";
                } else {
                    iurl = "https://partners.pnc.gob.sv/mci2/img/prm/" + timg + ".jpg";
                }
                il_pos.getInstance().displayImage(iurl, viewHolder.iv_pos, null, new ImageLoadingListener() {
                    @Override public void onLoadingStarted(String imageUri, View view) {}
                    @Override public void onLoadingFailed(String imageUri, View view, FailReason failReason) {}
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        String iusu = "p_" + viewHolder.tv_pid.getText().toString().trim() + ".png";
                        ContextWrapper wp = new ContextWrapper(mContext);
                        File file = wp.getDir("Images", Context.MODE_PRIVATE);
                        if(file.exists()){
                            File archivo =  new File(file, iusu);
                            if(!archivo.exists()) {
                                FileOutputStream outputStream = null;
                                try {
                                    outputStream = new FileOutputStream(archivo);
                                    loadedImage.compress(Bitmap.CompressFormat.PNG, 85, outputStream);
                                    outputStream.flush();
                                    outputStream.close();
                                } catch (Exception ex){}
                            }
                        } else {
                            file.mkdir();
                            File archivo =  new File(file, iusu);
                            FileOutputStream outputStream = null;
                            try {
                                outputStream = new FileOutputStream(archivo);
                                loadedImage.compress(Bitmap.CompressFormat.PNG, 85, outputStream);
                                outputStream.flush();
                                outputStream.close();
                            } catch (Exception ex){}
                        }
                    }
                    @Override public void onLoadingCancelled(String imageUri, View view) {}
                });

            }

            viewHolder.iv_pos.setVisibility(View.VISIBLE);
        }
        viewHolder.iv_tcm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int illv = viewHolder.wv_comment.getVisibility();
                if(illv == View.VISIBLE){
                    viewHolder.wv_comment.setVisibility(View.GONE);
                    viewHolder.ll_comment.setVisibility(View.GONE);
                } else {
                    viewHolder.wv_comment.setVisibility(View.VISIBLE);
                    viewHolder.ll_comment.setVisibility(View.VISIBLE);
                }
            }
        });
        srea = viewHolder.tv_ain.getText().toString().trim();
        viewHolder.tv_tcm.setText(post.tco);
        viewHolder.tv_comres.setText(post.tco + " Comentarios");
        viewHolder.tv_tmg.setText(post.tli);
        viewHolder.tv_comres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int illv = viewHolder.wv_comment.getVisibility();
                if(illv == View.VISIBLE){
                    viewHolder.wv_comment.setVisibility(View.GONE);
                    viewHolder.ll_comment.setVisibility(View.GONE);
                } else {
                    viewHolder.wv_comment.setVisibility(View.VISIBLE);
                    viewHolder.ll_comment.setVisibility(View.VISIBLE);
                }
            }
        });
        if(srea.equals("1")){
            viewHolder.iv_tmg.setImageResource(R.drawable.like);
        } else {
            viewHolder.iv_tmg.setImageResource(R.drawable.like_sg);
        }
        viewHolder.tv_tme.setText(post.tme);
        if(srea.equals("2")){
            viewHolder.iv_tme.setImageResource(R.drawable.love);
        } else {
            viewHolder.iv_tme.setImageResource(R.drawable.love_sg);
        }
        viewHolder.tv_tms.setText(post.tms);
        if(srea.equals("3")){
            viewHolder.iv_tms.setImageResource(R.drawable.surprise);
        } else {
            viewHolder.iv_tms.setImageResource(R.drawable.surprise_sg);
        }
        viewHolder.tv_tmm.setText(post.tmm);
        if(srea.equals("4")){
            viewHolder.iv_tmm.setImageResource(R.drawable.upset);
        } else {
            viewHolder.iv_tmm.setImageResource(R.drawable.upset_sg);
        }
        viewHolder.tv_tsa.setText(post.tsa);
        if(srea.equals("5")){
            viewHolder.iv_tsa.setImageResource(R.drawable.sad);
        } else {
            viewHolder.iv_tsa.setImageResource(R.drawable.sad_sg);
        }
        viewHolder.tv_tlo.setText(post.tlo);
        if(srea.equals("6")){
            viewHolder.iv_tlo.setImageResource(R.drawable.lol);
        } else {
            viewHolder.iv_tlo.setImageResource(R.drawable.lol_sg);
        }
        viewHolder.setNewReactionDisplay(Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tme.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tms.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim()));
        viewHolder.fl_react.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewHolder.ll_reactions.getVisibility() == View.VISIBLE){
                    viewHolder.ll_reactions.setVisibility(View.GONE);
                } else {
                    viewHolder.ll_reactions.setVisibility(View.VISIBLE);
                }
            }
        });
        viewHolder.fl_react.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                goToSR(viewHolder.tv_pid.getText().toString().trim());
                return true;
            }
        });
        viewHolder.iv_tmg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_ain = viewHolder.tv_ain.getText().toString().trim();
                int tot = 0;
                if(s_ain.equals("0") || s_ain.length() == 0){
                    tot = Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim());
                    tot += 1;
                    viewHolder.tv_tmg.setText(String.valueOf(tot));
                    dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "1", String.valueOf(tot));
                    viewHolder.tv_ain.setText("1");
                    viewHolder.iv_tmg.setImageResource(R.drawable.like);
                    viewHolder.ll_reactions.setVisibility(View.GONE);
                    gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "1", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                } else {
                    if(s_ain.equals("1")){
                        tot = Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim());
                        tot -= 1;
                        viewHolder.tv_tmg.setText(String.valueOf(tot));
                        dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "1", String.valueOf(tot));
                        viewHolder.tv_ain.setText("0");
                        viewHolder.iv_tmg.setImageResource(R.drawable.like_sg);
                        gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), s_ain, dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                        viewHolder.ll_reactions.setVisibility(View.GONE);
                    } else {
                        switch (s_ain){
                            case "2":
                                tot = Integer.parseInt(viewHolder.tv_tme.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tme.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tme.setImageResource(R.drawable.love_sg);
                                break;
                            case "3":
                                tot = Integer.parseInt(viewHolder.tv_tms.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tms.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tms.setImageResource(R.drawable.surprise_sg);
                                break;
                            case "4":
                                tot = Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tmm.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tmm.setImageResource(R.drawable.upset_sg);
                                break;
                            case "5":
                                tot = Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tsa.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tsa.setImageResource(R.drawable.sad_sg);
                                break;
                            case "6":
                                tot = Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tlo.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tlo.setImageResource(R.drawable.lol_sg);
                                break;
                            default:
                                break;
                        }
                        tot = Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim());
                        tot += 1;
                        viewHolder.tv_tmg.setText(String.valueOf(tot));
                        dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "1", String.valueOf(tot));
                        viewHolder.tv_ain.setText("1");
                        viewHolder.iv_tmg.setImageResource(R.drawable.like);
                        gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "1", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                        viewHolder.ll_reactions.setVisibility(View.GONE);
                    }
                }
                viewHolder.setNewReactionDisplay(Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tme.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tms.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim()));
            }
        });
        viewHolder.iv_tme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_ain = viewHolder.tv_ain.getText().toString().trim();
                int tot = 0;
                if(s_ain.equals("0") || s_ain.length() == 0){
                    tot = Integer.parseInt(viewHolder.tv_tme.getText().toString().trim());
                    tot += 1;
                    viewHolder.tv_tme.setText(String.valueOf(tot));
                    dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "2", String.valueOf(tot));
                    viewHolder.tv_ain.setText("2");
                    viewHolder.iv_tme.setImageResource(R.drawable.love);
                    gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "2", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                    viewHolder.ll_reactions.setVisibility(View.GONE);
                } else {
                    if(s_ain.equals("2")){
                        tot = Integer.parseInt(viewHolder.tv_tme.getText().toString().trim());
                        tot -= 1;
                        viewHolder.tv_tme.setText(String.valueOf(tot));
                        dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "2", String.valueOf(tot));
                        viewHolder.tv_ain.setText("0");
                        viewHolder.iv_tme.setImageResource(R.drawable.love_sg);
                        gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "0", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                        viewHolder.ll_reactions.setVisibility(View.GONE);
                    } else {
                        switch (s_ain){
                            case "1":
                                tot = Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tmg.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tmg.setImageResource(R.drawable.like_sg);
                                break;
                            case "3":
                                tot = Integer.parseInt(viewHolder.tv_tms.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tms.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tms.setImageResource(R.drawable.surprise_sg);
                                break;
                            case "4":
                                tot = Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tmm.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tmm.setImageResource(R.drawable.upset_sg);
                                break;
                            case "5":
                                tot = Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tsa.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tsa.setImageResource(R.drawable.sad_sg);
                                break;
                            case "6":
                                tot = Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tlo.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tlo.setImageResource(R.drawable.lol_sg);
                                break;
                            default:
                                break;
                        }
                        tot = Integer.parseInt(viewHolder.tv_tme.getText().toString().trim());
                        tot += 1;
                        viewHolder.tv_tme.setText(String.valueOf(tot));
                        dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "2", String.valueOf(tot));
                        viewHolder.tv_ain.setText("2");
                        viewHolder.iv_tme.setImageResource(R.drawable.love);
                        gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "2", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                        viewHolder.ll_reactions.setVisibility(View.GONE);
                    }
                }
                viewHolder.setNewReactionDisplay(Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tme.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tms.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim()));
            }
        });
        viewHolder.iv_tms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_ain = viewHolder.tv_ain.getText().toString().trim();
                int tot = 0;
                if(s_ain.equals("0") || s_ain.length() == 0){
                    tot = Integer.parseInt(viewHolder.tv_tms.getText().toString().trim());
                    tot += 1;
                    viewHolder.tv_tms.setText(String.valueOf(tot));
                    dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "3", String.valueOf(tot));
                    viewHolder.tv_ain.setText("3");
                    viewHolder.iv_tms.setImageResource(R.drawable.surprise);
                    gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "3", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                    viewHolder.ll_reactions.setVisibility(View.GONE);
                } else {
                    if(s_ain.equals("3")){
                        tot = Integer.parseInt(viewHolder.tv_tms.getText().toString().trim());
                        tot -= 1;
                        viewHolder.tv_tms.setText(String.valueOf(tot));
                        dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "3", String.valueOf(tot));
                        viewHolder.tv_ain.setText("0");
                        viewHolder.iv_tms.setImageResource(R.drawable.surprise_sg);
                        gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "0", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                        viewHolder.ll_reactions.setVisibility(View.GONE);
                    } else {
                        switch (s_ain){
                            case "1":
                                tot = Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tmg.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tmg.setImageResource(R.drawable.like_sg);
                                break;
                            case "2":
                                tot = Integer.parseInt(viewHolder.tv_tme.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tme.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tme.setImageResource(R.drawable.love_sg);
                                break;
                            case "4":
                                tot = Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tmm.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tmm.setImageResource(R.drawable.upset_sg);
                                break;
                            case "5":
                                tot = Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tsa.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tsa.setImageResource(R.drawable.sad_sg);
                                break;
                            case "6":
                                tot = Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tlo.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tlo.setImageResource(R.drawable.lol_sg);
                                break;
                            default:
                                break;
                        }
                        tot = Integer.parseInt(viewHolder.tv_tms.getText().toString().trim());
                        tot += 1;
                        viewHolder.tv_tms.setText(String.valueOf(tot));
                        dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "3", String.valueOf(tot));
                        viewHolder.tv_ain.setText("3");
                        viewHolder.iv_tms.setImageResource(R.drawable.surprise);
                        gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "3", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                        viewHolder.ll_reactions.setVisibility(View.GONE);
                    }
                }
                viewHolder.setNewReactionDisplay(Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tme.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tms.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim()));
            }
        });
        viewHolder.iv_tmm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_ain = viewHolder.tv_ain.getText().toString().trim();
                int tot = 0;
                if(s_ain.equals("0") || s_ain.length() == 0){
                    tot = Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim());
                    tot += 1;
                    viewHolder.tv_tmm.setText(String.valueOf(tot));
                    dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "4", String.valueOf(tot));
                    viewHolder.tv_ain.setText("4");
                    viewHolder.iv_tmm.setImageResource(R.drawable.upset);
                    gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "4", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                    viewHolder.ll_reactions.setVisibility(View.GONE);
                } else {
                    if(s_ain.equals("4")){
                        tot = Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim());
                        tot -= 1;
                        viewHolder.tv_tmm.setText(String.valueOf(tot));
                        dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "4", String.valueOf(tot));
                        viewHolder.tv_ain.setText("0");
                        viewHolder.iv_tmm.setImageResource(R.drawable.upset_sg);
                        gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "0", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                        viewHolder.ll_reactions.setVisibility(View.GONE);
                    } else {
                        switch (s_ain){
                            case "1":
                                tot = Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tmg.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tmg.setImageResource(R.drawable.like_sg);
                                break;
                            case "2":
                                tot = Integer.parseInt(viewHolder.tv_tme.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tme.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tme.setImageResource(R.drawable.love_sg);
                                break;
                            case "3":
                                tot = Integer.parseInt(viewHolder.tv_tms.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tms.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tms.setImageResource(R.drawable.surprise_sg);
                                break;
                            case "5":
                                tot = Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tsa.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tsa.setImageResource(R.drawable.sad_sg);
                                break;
                            case "6":
                                tot = Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tlo.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tlo.setImageResource(R.drawable.lol_sg);
                                break;
                            default:
                                break;
                        }
                        tot = Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim());
                        tot += 1;
                        viewHolder.tv_tmm.setText(String.valueOf(tot));
                        dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "4", String.valueOf(tot));
                        viewHolder.tv_ain.setText("4");
                        viewHolder.iv_tmm.setImageResource(R.drawable.upset);
                        gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "4", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                        viewHolder.ll_reactions.setVisibility(View.GONE);
                    }
                }
                viewHolder.setNewReactionDisplay(Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tme.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tms.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim()));
            }
        });
        viewHolder.iv_tsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_ain = viewHolder.tv_ain.getText().toString().trim();
                int tot = 0;
                if(s_ain.equals("0") || s_ain.length() == 0){
                    tot = Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim());
                    tot += 1;
                    viewHolder.tv_tsa.setText(String.valueOf(tot));
                    dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "5", String.valueOf(tot));
                    viewHolder.tv_ain.setText("5");
                    viewHolder.iv_tsa.setImageResource(R.drawable.sad);
                    gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "5", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                    viewHolder.ll_reactions.setVisibility(View.GONE);
                } else {
                    if(s_ain.equals("5")){
                        tot = Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim());
                        tot -= 1;
                        viewHolder.tv_tsa.setText(String.valueOf(tot));
                        dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "5", String.valueOf(tot));
                        viewHolder.tv_ain.setText("0");
                        viewHolder.iv_tsa.setImageResource(R.drawable.sad_sg);
                        gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "0", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                        viewHolder.ll_reactions.setVisibility(View.GONE);
                    } else {
                        switch (s_ain){
                            case "1":
                                tot = Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tmg.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tmg.setImageResource(R.drawable.like_sg);
                                break;
                            case "2":
                                tot = Integer.parseInt(viewHolder.tv_tme.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tme.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tme.setImageResource(R.drawable.love_sg);
                                break;
                            case "3":
                                tot = Integer.parseInt(viewHolder.tv_tms.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tms.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tms.setImageResource(R.drawable.surprise_sg);
                                break;
                            case "4":
                                tot = Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tmm.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tmm.setImageResource(R.drawable.upset_sg);
                                break;
                            case "6":
                                tot = Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tlo.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tlo.setImageResource(R.drawable.lol_sg);
                                break;
                            default:
                                break;
                        }
                        tot = Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim());
                        tot += 1;
                        viewHolder.tv_tsa.setText(String.valueOf(tot));
                        dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "5", String.valueOf(tot));
                        viewHolder.tv_ain.setText("5");
                        viewHolder.iv_tsa.setImageResource(R.drawable.sad);
                        gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "5", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                        viewHolder.ll_reactions.setVisibility(View.GONE);
                    }
                }
                viewHolder.setNewReactionDisplay(Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tme.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tms.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim()));
            }
        });

        viewHolder.iv_tlo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_ain = viewHolder.tv_ain.getText().toString().trim();
                int tot = 0;
                if(s_ain.equals("0") || s_ain.length() == 0){
                    tot = Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim());
                    tot += 1;
                    viewHolder.tv_tlo.setText(String.valueOf(tot));
                    dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "6", String.valueOf(tot));
                    viewHolder.tv_ain.setText("6");
                    viewHolder.iv_tlo.setImageResource(R.drawable.lol);
                    gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "6", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                    viewHolder.ll_reactions.setVisibility(View.GONE);
                } else {
                    if(s_ain.equals("6")){
                        tot = Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim());
                        tot -= 1;
                        viewHolder.tv_tlo.setText(String.valueOf(tot));
                        dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "6", String.valueOf(tot));
                        viewHolder.tv_ain.setText("0");
                        viewHolder.iv_tlo.setImageResource(R.drawable.lol_sg);
                        gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "0", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                        viewHolder.ll_reactions.setVisibility(View.GONE);
                    } else {
                        switch (s_ain){
                            case "1":
                                tot = Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tmg.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tmg.setImageResource(R.drawable.like_sg);
                                break;
                            case "2":
                                tot = Integer.parseInt(viewHolder.tv_tme.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tme.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tme.setImageResource(R.drawable.love_sg);
                                break;
                            case "3":
                                tot = Integer.parseInt(viewHolder.tv_tms.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tms.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tms.setImageResource(R.drawable.surprise_sg);
                                break;
                            case "4":
                                tot = Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tmm.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tmm.setImageResource(R.drawable.upset_sg);
                                break;
                            case "5":
                                tot = Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim());
                                tot -= 1;
                                viewHolder.tv_tsa.setText(String.valueOf(tot));
                                dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), s_ain, String.valueOf(tot));
                                viewHolder.iv_tsa.setImageResource(R.drawable.sad_sg);
                                break;
                            default:
                                break;
                        }
                        tot = Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim());
                        tot += 1;
                        viewHolder.tv_tlo.setText(String.valueOf(tot));
                        dba.updateReactionCounter(viewHolder.tv_pid.getText().toString().trim(), "6", String.valueOf(tot));
                        viewHolder.tv_ain.setText("6");
                        viewHolder.iv_tlo.setImageResource(R.drawable.lol);
                        gu.reactToPost(viewHolder.tv_pid.getText().toString().trim(), "6", dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                        viewHolder.ll_reactions.setVisibility(View.GONE);
                    }
                }
                viewHolder.setNewReactionDisplay(Integer.parseInt(viewHolder.tv_tmg.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tme.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tms.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tmm.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tsa.getText().toString().trim()), Integer.parseInt(viewHolder.tv_tlo.getText().toString().trim()));
            }
        });

        html = new StringBuilder();
        html.append("<div>");
        coms = dba.pullCommentsByPID(viewHolder.tv_pid.getText().toString().trim());
        if(coms.getCount() > 0){
            while(coms.moveToNext()){
                html.append("<font size='-1' color='#026b42'><i><b>" + coms.getString(0) + "</b></i> respondio el " + coms.getString(1).substring(0, 14) + "</font><br /><font size='-1' color='#0b3f8b'>" + coms.getString(2) + "</font><br /><br />");
            }
            coms.close();
        } else {
            html.append("<font size='-1'><i>NO HAY COMENTARIOS</i><br />Se el primero en comentar!</font>");
        }
        viewHolder.wv_comment.loadData(html.toString(), "text/html; charset=utf-8", "UTF-8");
        viewHolder.iv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(gu.isNetworkAvailable()){
                    String men = viewHolder.et_comment.getText().toString().trim();
                    if(men.length() > 0){
                        Date date = new Date();
                        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm");
                        String fec = df.format(date);
                        gu.commentPost(viewHolder.tv_pid.getText().toString().trim(), men, dba.getParamByName("dui"), "0.0.0.0", dba.getParamByName("oni"), dba.getParamByName("nic"), viewHolder.tv_usu.getText().toString().trim());
                        dba.addCommentsToLocalDB("0", viewHolder.tv_pid.getText().toString().trim(), dba.getParamByName("nic"), fec, men);
                        html = new StringBuilder();
                        html.append("<div>");
                        coms = dba.pullCommentsByPID(viewHolder.tv_pid.getText().toString().trim());
                        int it = coms.getCount();
                        if(coms.getCount() > 0){
                            while(coms.moveToNext()){
                                html.append("<font size='-1' color='#026b42'><i><b>" + coms.getString(0) + "</b></i> respondio el " + coms.getString(1).substring(0, 14) + "</font><br /><font size='-1' color='#0b3f8b'>" + coms.getString(2) + "</font><br /><br />");
                            }
                            coms.close();
                        } else {
                            html.append("<font size='-1'><i>NO HAY COMENTARIOS</i><br />Se el primero en comentar!</font>");
                        }
                        viewHolder.tv_comres.setText(String.valueOf(it) + " Comentarios");
                        coms.close();
                        viewHolder.wv_comment.loadData(html.toString(), "text/html; charset=utf-8", "UTF-8");
                        viewHolder.ll_comment.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(mContext, "Debes escribir un comentario para publicar", Toast.LENGTH_LONG).show();
                        //gu.displayAlert("INCOMPLETO", "Debes escribir un comentario para publicar", mContext);
                    }
                } else {
                    Toast.makeText(mContext, "Partners App requiere una conexion activa a Internet", Toast.LENGTH_LONG).show();
                    //gu.displayAlert("SIN CONEXION", "Partners App requiere una conexion activa a Internet", mContext);
                }
            }
        });

    }

    public void goToSR(String pid){
        Intent i = new Intent(mContext, ShowReactionsActivity.class);
        i.putExtra("pid", pid);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(i);
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView tv_usu, tv_fec, tv_tit, tv_pos, tv_tcm, tv_tmg, tv_tme, tv_tmm, tv_tms, tv_tsa, tv_tlo, tv_oni, tv_did, tv_pid, tv_ain, tv_newtcm;
        private TextView tv_imgurl, tv_prourl, tv_comres;
        private WebView wv_comment;
        private LinearLayout ll_comment;
        private LinearLayout ll_reactions;
        private FrameLayout fl_react;
        private EditText et_comment;
        private ImageView iv_pro, iv_pos, iv_lnk, iv_tcm, iv_tmg, iv_tme, iv_tmm, iv_tms, iv_tsa, iv_tlo, iv_ori, iv_send, iv_newp1, iv_newp2, iv_newp3, iv_newp4, iv_newp5, iv_newp6;
        //faltan variables
        public ViewHolder(View view) {
            super(view);
            iv_pro = (ImageView)view.findViewById(R.id.imageView54);
            iv_pos = (ImageView)view.findViewById(R.id.imageView56);
            iv_lnk = (ImageView)view.findViewById(R.id.iv_linkcomment);
            iv_tcm = (ImageView)view.findViewById(R.id.imageView57);
            iv_tmg = (ImageView)view.findViewById(R.id.imageView58);
            iv_tme = (ImageView)view.findViewById(R.id.imageView59);
            iv_tmm = (ImageView)view.findViewById(R.id.imageView61);
            iv_tms = (ImageView)view.findViewById(R.id.imageView60);
            iv_tsa = (ImageView)view.findViewById(R.id.imageView63);
            iv_tlo = (ImageView)view.findViewById(R.id.imageView64);
            iv_ori = (ImageView)view.findViewById(R.id.imageView62);
            iv_send = (ImageView)view.findViewById(R.id.imageView15);
            iv_newp1 = (ImageView)view.findViewById(R.id.imageView16);
            iv_newp2 = (ImageView)view.findViewById(R.id.imageView17);
            iv_newp3 = (ImageView)view.findViewById(R.id.imageView18);
            iv_newp4 = (ImageView)view.findViewById(R.id.imageView19);
            iv_newp5 = (ImageView)view.findViewById(R.id.imageView20);
            iv_newp6 = (ImageView)view.findViewById(R.id.imageView21);
            tv_usu = (TextView)view.findViewById(R.id.textView403);
            tv_fec = (TextView)view.findViewById(R.id.textView404);
            tv_tit = (TextView)view.findViewById(R.id.textView405);
            tv_pos = (TextView)view.findViewById(R.id.textView406);
            tv_tcm = (TextView)view.findViewById(R.id.textView407);
            tv_tmg = (TextView)view.findViewById(R.id.textView408);
            tv_tme = (TextView)view.findViewById(R.id.textView409);
            tv_tms = (TextView)view.findViewById(R.id.textView411);
            tv_tmm = (TextView)view.findViewById(R.id.textView412);
            tv_tsa = (TextView)view.findViewById(R.id.textView413);
            tv_tlo = (TextView)view.findViewById(R.id.textView418);
            tv_oni = (TextView)view.findViewById(R.id.textView414);
            tv_did = (TextView)view.findViewById(R.id.textView415);
            tv_pid = (TextView)view.findViewById(R.id.textView416);
            tv_ain = (TextView)view.findViewById(R.id.textView417);
            tv_newtcm = (TextView)view.findViewById(R.id.textView2);
            tv_imgurl = (TextView)view.findViewById(R.id.textView19);
            tv_prourl = (TextView)view.findViewById(R.id.textView20);
            tv_comres = (TextView)view.findViewById(R.id.textView21);
            wv_comment = (WebView)view.findViewById(R.id.wv_comment);
            ll_comment = (LinearLayout)view.findViewById(R.id.ll_comment);
            ll_reactions = (LinearLayout)view.findViewById(R.id.ll_reactions);
            et_comment = (EditText)view.findViewById(R.id.editText4);
            fl_react = (FrameLayout)view.findViewById(R.id.fl_newrac);

            et_comment.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

            view.setOnClickListener(this);
        }

        private void setNewReactionDisplay(int like, int love, int surprise, int angry, int sad, int lol){
            int mainTotal = 0;
            int cp = 0;
            iv_newp1.setImageResource(R.drawable.like_sg);
            ImageView iv_temp;
            iv_newp1.setVisibility(View.GONE);
            iv_newp2.setVisibility(View.GONE);
            iv_newp3.setVisibility(View.GONE);
            iv_newp4.setVisibility(View.GONE);
            iv_newp5.setVisibility(View.GONE);
            iv_newp6.setVisibility(View.GONE);

            iv_temp = iv_newp1;
            if(like > 0){
                iv_temp.setVisibility(View.VISIBLE);
                iv_temp.setImageResource(R.drawable.like);
                mainTotal += like;
                cp += 1;
            } else {
                iv_temp.setVisibility(View.VISIBLE);
                iv_temp.setImageResource(R.drawable.like_sg);
            }
            if(love > 0){
                if(cp != 0){
                    iv_temp = iv_newp2;
                } else {
                    iv_temp = iv_newp1;
                }
                iv_temp.setVisibility(View.VISIBLE);
                iv_temp.setImageResource(R.drawable.love);
                mainTotal += love;
                cp += 1;
            }
            if(surprise > 0){
                switch (cp){
                    case 0:
                        iv_temp = iv_newp1;
                        break;
                    case 1:
                        iv_temp = iv_newp2;
                        break;
                    case 2:
                        iv_temp = iv_newp3;
                        break;
                    default:
                        break;
                }
                iv_temp.setVisibility(View.VISIBLE);
                iv_temp.setImageResource(R.drawable.surprise);
                mainTotal += surprise;
                cp += 1;
            }
            if(angry > 0){
                switch (cp){
                    case 0:
                        iv_temp = iv_newp1;
                        break;
                    case 1:
                        iv_temp = iv_newp2;
                        break;
                    case 2:
                        iv_temp = iv_newp3;
                        break;
                    case 3:
                        iv_temp = iv_newp4;
                        break;
                    default:
                        break;

                }
                iv_temp.setVisibility(View.VISIBLE);
                iv_temp.setImageResource(R.drawable.upset);
                mainTotal += angry;
                cp += 1;
            }
            if(sad> 0){
                switch (cp){
                    case 0:
                        iv_temp = iv_newp1;
                        break;
                    case 1:
                        iv_temp = iv_newp2;
                        break;
                    case 2:
                        iv_temp = iv_newp3;
                        break;
                    case 3:
                        iv_temp = iv_newp4;
                        break;
                    case 4:
                        iv_temp = iv_newp5;
                        break;
                    default:
                        break;

                }
                iv_temp.setVisibility(View.VISIBLE);
                iv_temp.setImageResource(R.drawable.sad);
                mainTotal += sad;
                cp += 1;
            }
            if(lol> 0){
                switch (cp){
                    case 0:
                        iv_temp = iv_newp1;
                        break;
                    case 1:
                        iv_temp = iv_newp2;
                        break;
                    case 2:
                        iv_temp = iv_newp3;
                        break;
                    case 3:
                        iv_temp = iv_newp4;
                        break;
                    case 4:
                        iv_temp = iv_newp5;
                        break;
                    case 5:
                        iv_temp = iv_newp6;
                        break;
                    default:
                        break;

                }
                iv_temp.setVisibility(View.VISIBLE);
                iv_temp.setImageResource(R.drawable.lol);
                mainTotal += lol;
                cp += 1;
            }

            if(mainTotal == 0){
                //iv_newtmg.setVisibility(View.VISIBLE);
                tv_newtcm.setVisibility(View.GONE);
            } else {
                tv_newtcm.setText(String.valueOf(mainTotal));
                tv_newtcm.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onClick(View v){
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }
}
