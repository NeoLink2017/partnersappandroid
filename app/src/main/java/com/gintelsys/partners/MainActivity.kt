package com.gintelsys.partners

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.telephony.TelephonyManager
import java.util.*
import com.gintelsys.partners.utils.GeneralUtils
import com.gintelsys.partners.data.dbHandler
import okhttp3.OkHttpClient
import com.gintelsys.partners.network.OkHttpRequest

import java.lang.Exception
import kotlin.collections.HashMap
import android.graphics.Typeface
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException


class MainActivity : AppCompatActivity() {
    private lateinit var gu : GeneralUtils
    val dba = dbHandler(this, null, null, 1)
    val PERMISSION_ALL = 1
    internal var allperok = false
    internal var IMEI = ""
    var client = OkHttpClient()
    var request = OkHttpRequest(client)
    var s_res = ""
    var s_men = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        gu = GeneralUtils(this)

        val typefaceBold = Typeface.createFromAsset(assets, "Volkhov-Bold.ttf")
        val typefaceNormal = Typeface.createFromAsset(assets, "Volkhov.ttf")

        txtTitulo.typeface = typefaceBold
        txtProgress.typeface = typefaceNormal
        textView.typeface = typefaceNormal

        gu.setRestServer("production")

        if(checkAndRequestPermissions()){
            allperok = true
            contineExecution()
        }
    }

    fun checkAndRequestPermissions(): Boolean {
        val p1 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
        val p2 = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
        val p3 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE)
        val p4 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE)
        val p5 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val p6 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)

        val listPermissionsNeeded = ArrayList<String>()

        if (p1 != PackageManager.PERMISSION_GRANTED) {listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE)}
        if (p2 != PackageManager.PERMISSION_GRANTED) {listPermissionsNeeded.add(Manifest.permission.INTERNET)}
        if (p3 != PackageManager.PERMISSION_GRANTED) {listPermissionsNeeded.add(Manifest.permission.ACCESS_WIFI_STATE)}
        if (p4 != PackageManager.PERMISSION_GRANTED) {listPermissionsNeeded.add(Manifest.permission.ACCESS_NETWORK_STATE)}
        if (p5 != PackageManager.PERMISSION_GRANTED) {listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)}
        if (p6 != PackageManager.PERMISSION_GRANTED) {listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)}

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toTypedArray(), PERMISSION_ALL)
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        val listPermissionsNeeded = ArrayList<String>()

        val p1 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
        val p2 = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
        val p3 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE)
        val p4 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE)
        val p5 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val p6 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)

        if (p1 != PackageManager.PERMISSION_GRANTED) {listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE)}
        if (p2 != PackageManager.PERMISSION_GRANTED) {listPermissionsNeeded.add(Manifest.permission.INTERNET)}
        if (p3 != PackageManager.PERMISSION_GRANTED) {listPermissionsNeeded.add(Manifest.permission.ACCESS_WIFI_STATE)}
        if (p4 != PackageManager.PERMISSION_GRANTED) {listPermissionsNeeded.add(Manifest.permission.ACCESS_NETWORK_STATE)}
        if (p5 != PackageManager.PERMISSION_GRANTED) {listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)}
        if (p6 != PackageManager.PERMISSION_GRANTED) {listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)}

        if (listPermissionsNeeded.isEmpty()) {
            allperok = true
            contineExecution()
        } else {
            allperok = false
            txtProgress.text = "Partners App requiere los permisos solicitados para poder operar con normalidad."
            pbProgress.visibility = View.INVISIBLE
        }
    }

    fun contineExecution() {
        if(allperok == true){
            Handler().postDelayed({
                if(dba.hasAccount()){
                    goToHome()
                } else {
                    if(gu.isNetworkAvailable){
                        doXpressLogin()
                    } else {
                        gu.displayAlert("SIN ACCESO A INTERNET", "Partners App requiere acceso a Internet. Verifica tu conexion o revisa si esta App tiene Permisos para Internet", this)
                    }
                }
            }, 500)
        } else {
            gu.displayAlert("PERMISOS REQUERIDOS", "Partners no puede iniciar hasta que hayas concedido los permisos necesarios para funcionar correctamente", this)
        }
    }

    fun doXpressLogin() {
        pbProgress.visibility = View.VISIBLE
        txtProgress.visibility = View.VISIBLE

        val url = gu.getApiRestURL("xlog")
        val vals: HashMap<String, String> = hashMapOf("v1" to getDeviceImei())

        request.POST(url, vals, object: Callback {
            override fun onResponse(call: Call?, response: Response) {
                val responseData = response.body()?.string()
                runOnUiThread{
                    try {
                        var json = JSONObject(responseData)
                        var jsdats = json.getJSONArray("dats")
                        var jsdtArrayObject = jsdats.getJSONObject(0)

                        s_res = jsdtArrayObject.getString("res")
                        s_men = jsdtArrayObject.getString("men")

                        //Log.i("VIEWDATA", getDeviceImei())

                        if(s_res.equals("1")){
                            var jsdatarray = jsdtArrayObject.getJSONArray("dtarray")
                            var jsdtrow = jsdatarray.getJSONObject(0)

                            var k_res = jsdtrow.getString("res")
                            var k_men = jsdtrow.getString("men")

                            if(k_res.equals("1")){
                                var k_sid = jsdtrow.getString("sid")
                                var k_oni = jsdtrow.getString("oni")
                                var k_nom = jsdtrow.getString("nom")
                                var k_est = jsdtrow.getString("est")
                                var k_nic = jsdtrow.getString("nic")
                                var k_dui = jsdtrow.getString("dui")

                                dba.addCuentaItem("sid", k_sid)
                                dba.addCuentaItem("oni", k_oni)
                                dba.addCuentaItem("nom", k_nom)
                                dba.addCuentaItem("nic", k_nic)
                                dba.addCuentaItem("est", k_est)
                                dba.addCuentaItem("unidevid", getDeviceImei())
                                dba.addCuentaItem("dui", k_dui)
                                dba.addCuentaItem("fbid", "")

                                goToHome()
                            } else {
                                if(gu.isNetworkAvailable){
                                    goToLogin()
                                } else {
                                    gu.displayAlert("SIN ACCESO A INTERNET", "Partners App requiere acceso a Internet. Verifica tu conexion o revisa si esta App tiene Permisos para Internet", applicationContext)
                                }
                            }

                        } else {
                            txtProgress.setText("FAIL")
                        }
                    } catch (e: JSONException) {
                        //Log.i("VIEWDATA",  e.message)
                    }
                }
            }

            override fun onFailure(call: Call?, e: IOException?) {
                //Log.i("VIEWDATA",  e.toString())
            }
        })
    }

    fun getDeviceImei(): String {
        try{
            val tm = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_PHONE_STATE)) {
                    IMEI = tm.getDeviceId()
                } else {
                    return "NOT_FOUNT"
                }
            }

            if (IMEI != null){
                return Settings.Secure.getString(applicationContext.contentResolver, Settings.Secure.ANDROID_ID)
            } else {
                return "NOT_FOUND"
            }
        } catch (ex:Exception){
            return "NOT_FOUND"
        }
    }

    fun goToLogin() {
        val i = Intent(applicationContext, LoginActivity::class.java)
        startActivity(i)
    }

    fun goToHome() {
        val i = Intent(applicationContext, PINActivity::class.java)
        startActivity(i)
    }
}
