package com.gintelsys.partners.adaptes;

public class ReactionItem {
    public String pid, rea, nic, ava;

    public ReactionItem(String pid, String rea, String nic, String ava){
        this.pid = pid;
        this.rea = rea;
        this.nic = nic;
        this.ava = ava;
    }
}
