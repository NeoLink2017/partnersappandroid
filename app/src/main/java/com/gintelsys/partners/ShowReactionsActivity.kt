package com.gintelsys.partners

import android.database.Cursor
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.gintelsys.partners.adaptes.ReactionAdapter
import com.gintelsys.partners.adaptes.ReactionItem
import com.gintelsys.partners.data.dbHandler
import kotlinx.android.synthetic.main.activity_showreactions.*

class ShowReactionsActivity : AppCompatActivity() {
    val unicode = 0x000AB
    lateinit var dba: dbHandler
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: ReactionAdapter
    lateinit var items: ArrayList<ReactionItem>
    lateinit var item: ReactionItem
    var lik = 0
    var lov = 0
    var wow = 0
    var ups = 0
    var sad = 0
    var lol = 0
    var pid = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_showreactions)

        dba = dbHandler(this, null, null, 1)
        val back: String = String(Character.toChars(unicode))
        pid = intent.getStringExtra("pid")
        textView22.text =  " " + back + " " + back + " | Partners que han reaccionado:"


        textView22.setOnClickListener(View.OnClickListener {
            finish()
        })

        initLayout()
    }

    fun initLayout() {
        linearLayoutManager = LinearLayoutManager(this)
        rv_reactions.layoutManager = linearLayoutManager
        items = ArrayList<ReactionItem>()
        adapter = ReactionAdapter(items, applicationContext)
        rv_reactions.adapter = adapter

        var c : Cursor = dba.pullReactionsByPID(pid);
        while(c.moveToNext()){
            item = ReactionItem(pid, c.getString(0), c.getString(1), c.getString(2))
            when(c.getString(0)){
                "1" -> lik += 1
                "2" -> lov += 1
                "3" -> wow += 1
                "4" -> ups += 1
                "5" -> sad += 1
                "6" -> lol += 1
                else -> {}
            }
            items.add(item)
        }
        c.close()

        adapter.notifyDataSetChanged()

        textView25.text = lik.toString()
        textView26.text = lov.toString()
        textView27.text = wow.toString()
        textView28.text = ups.toString()
        textView29.text = sad.toString()
        textView30.text = lol.toString()
        textView23.text = "Todas " + (lik + lov + wow + ups + sad + lol).toString()
    }
}