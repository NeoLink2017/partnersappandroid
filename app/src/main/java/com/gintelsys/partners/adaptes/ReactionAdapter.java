package com.gintelsys.partners.adaptes;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.gintelsys.partners.R;
import com.gintelsys.partners.data.dbHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class ReactionAdapter extends RecyclerView.Adapter<ReactionAdapter.ViewHolder> {
    private ArrayList<ReactionItem> reactionList  = new ArrayList<>();
    ReactionItem item;
    OnItemClickListener mItemClickListener;
    ImageLoader il_pro;
    Context mContext;
    dbHandler dba;

    public ReactionAdapter(ArrayList<ReactionItem> reactionList, Context context){
        this.reactionList = reactionList;
        this.mContext = context;
        dba = new dbHandler(context, null, null, 1);

        il_pro = ImageLoader.getInstance();
        il_pro.init(ImageLoaderConfiguration.createDefault(context));
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItenClickListener) {
        this.mItemClickListener = mItenClickListener;
    }

    @Override
    public ReactionAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_showreactions, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ReactionAdapter.ViewHolder viewHolder, int i){
        item = reactionList.get(i);

        viewHolder.tv_usu.setText(item.nic);
        viewHolder.tv_pid.setText(item.ava);
        switch (item.rea){
            case "1":
                viewHolder.iv_rea.setImageResource(R.drawable.like);
                break;
            case "2":
                viewHolder.iv_rea.setImageResource(R.drawable.love);
                break;
            case "3":
                viewHolder.iv_rea.setImageResource(R.drawable.surprise);
                break;
            case "4":
                viewHolder.iv_rea.setImageResource(R.drawable.upset);
                break;
            case "5":
                viewHolder.iv_rea.setImageResource(R.drawable.sad);
                break;
            case "6":
                viewHolder.iv_rea.setImageResource(R.drawable.lol);
                break;
            default:
                break;
        }

        String iusu = viewHolder.tv_usu.getText().toString().trim() + ".png";
        ContextWrapper wp = new ContextWrapper(mContext);
        File file = wp.getDir("Images", Context.MODE_PRIVATE);
        File archivo =  new File(file, iusu);
        if(archivo.exists()){
            String lsd = archivo.getPath();
            viewHolder.iv_pro.setImageBitmap(BitmapFactory.decodeFile(lsd));
        } else {
            String pimg = viewHolder.tv_pid.getText().toString().trim();
            String iurl = "";
            if(pimg.length() > 40){
                iurl = "https://imperium.pnc.gob.sv/apis/prtnrs/?dp=imahan&v1=" + pimg + "&v2=PRO";
            } else {
                iurl = "https://partners.pnc.gob.sv/mci2/avatar/" + pimg;
            }
            il_pro.displayImage(iurl, viewHolder.iv_pro, null, new ImageLoadingListener() {
                @Override public void onLoadingStarted(String imageUri, View view) {}
                @Override public void onLoadingFailed(String imageUri, View view, FailReason failReason) {}
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    String iusu = item.nic + ".png";
                    ContextWrapper wp = new ContextWrapper(mContext);
                    File file = wp.getDir("Images", Context.MODE_PRIVATE);
                    if(file.exists()){
                        File archivo =  new File(file, iusu);
                        if(!archivo.exists()) {
                            FileOutputStream outputStream = null;
                            try {
                                outputStream = new FileOutputStream(archivo);
                                loadedImage.compress(Bitmap.CompressFormat.PNG, 85, outputStream);
                                outputStream.flush();
                                outputStream.close();
                            } catch (Exception ex){}
                        }
                    } else {
                        file.mkdir();
                        File archivo =  new File(file, iusu);
                        FileOutputStream outputStream = null;
                        try {
                            outputStream = new FileOutputStream(archivo);
                            loadedImage.compress(Bitmap.CompressFormat.PNG, 85, outputStream);
                            outputStream.flush();
                            outputStream.close();
                        } catch (Exception ex){}
                    }
                }
                @Override public void onLoadingCancelled(String imageUri, View view) {}
            });
        }
    }

    @Override
    public int getItemCount() {
        return reactionList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView tv_usu, tv_pid;
        private ImageView iv_pro, iv_rea;
        private Button btn_add;

        public ViewHolder(View view) {
            super(view);

            iv_pro = (ImageView)view.findViewById(R.id.imageView24);
            iv_rea = (ImageView)view.findViewById(R.id.imageView26);
            tv_usu = (TextView)view.findViewById(R.id.textView24);
            tv_pid = (TextView)view.findViewById(R.id.textView29);
            btn_add = (Button)view.findViewById(R.id.button2);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v){
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }
}
