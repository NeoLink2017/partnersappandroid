package com.gintelsys.partners.adaptes;

public class PostItem {
    public String pid, typ, usu, ava, fec, ori, him, tit, img, men, tco, tli, tme, tms, tmm, idr, sto, rea, tsa, tlo;

    public PostItem(String pid, String typ, String usu, String ava, String fec, String ori, String him, String tit, String img, String men, String tco, String tli, String tme, String tms, String tmm, String idr, String sto, String rea, String tsa, String tlo) {
        this.pid = pid;
        this.typ = typ;
        this.usu = usu;
        this.ava = ava;
        this.fec = fec;
        this.ori = ori;
        this.him = him;
        this.tit = tit;
        this.img = img;
        this.men = men;
        this.tco = tco;
        this.tli = tli;
        this.tme = tme;
        this.tms = tms;
        this.tmm = tmm;
        this.idr = idr;
        this.sto = sto;
        this.rea = rea;
        this.tsa = tsa;
        this.tlo = tlo;
    }
}
