package com.gintelsys.partners

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.text.InputFilter
import android.util.Base64
import android.util.Log
import android.view.View
import com.gintelsys.partners.data.dbHandler
import com.gintelsys.partners.network.OkHttpRequest
import com.gintelsys.partners.utils.GeneralUtils
import kotlinx.android.synthetic.main.activity_publish.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.lang.StringBuilder

class PublishActivity : AppCompatActivity() {
    private val unicode = 0x000AB
    var mCurrentPhotoPath = ""
    private var TAKE_PHOTO_REQUEST = 1
    private var IMAGE_PICK_CODE = 1000
    var hasPhoto = false
    var pos = ""
    var tit = ""
    var gu = GeneralUtils(this)
    private val dba = dbHandler(this, null, null, 1)
    var client = OkHttpClient()
    var request = OkHttpRequest(client)
    var s_res = ""
    var s_men = ""
    var dIP = ""
    var dMac = ""
    var b64 = StringBuilder()
    var fname = ""
    var dname = ""
    var postok = ""
    lateinit var wp : ContextWrapper
    lateinit var file : File
    lateinit var archivo : File

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_publish)

        gu.setRestServer("production")
        wp = ContextWrapper(applicationContext)
        file = wp.getDir("tmpdata", Context.MODE_PRIVATE)

        val back: String = String(Character.toChars(unicode))
        textView33.text =  " " + back + " " + back + " | Regresar a Publicaciones:"

        editText5.setFilters(arrayOf<InputFilter>(InputFilter.AllCaps()))
        editText6.setFilters(arrayOf<InputFilter>(InputFilter.AllCaps()))

        textView33.setOnClickListener(View.OnClickListener {
            finish()
        })
        imageView27.setOnClickListener(View.OnClickListener {
            launchCamera()
        })
        imageView28.setOnClickListener(View.OnClickListener {
            pickImageFromGallery()
        })
        imageView30.setOnClickListener(View.OnClickListener {
            if(hasPhoto){
                hasPhoto = false
                frameLayout.visibility = View.GONE
                linearLayout8.visibility = View.VISIBLE
                linearLayout7.visibility = View.VISIBLE
                b64 = StringBuilder()
            }
        })
        button4.setOnClickListener(View.OnClickListener {
            if(validate()){
                progressBar2.visibility = View.VISIBLE
                textView40.visibility = View.VISIBLE
                button4.isEnabled = false
                if(hasPhoto){
                    dname = "SINARCHIVO"
                    sendImageToDocsServer()
                } else {
                    fname = "SINIMAGEN"
                    dname = "SINARCHIVO"
                    publishMessage()
                }
            }
        })
    }

    fun validate() : Boolean {
        var r = true
        textView39.text = ""

        tit = editText5.text.toString().trim()
        pos = editText6.text.toString().trim()

        if(pos.isEmpty()){
            textView39.text = "DEBES PUBLICAR UN MENSAJE"
            return false
        }

        return r
    }

    private fun launchCamera() {
        val values = ContentValues(1)
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
        val fileUri = contentResolver
            .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values)
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if(intent.resolveActivity(packageManager) != null) {
            mCurrentPhotoPath = fileUri.toString()
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                    or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            startActivityForResult(intent, TAKE_PHOTO_REQUEST)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == TAKE_PHOTO_REQUEST) {
            processCapturedPhoto()
        } else if(resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            imageView29.setImageURI(data?.data)

            hasPhoto = true
            frameLayout.visibility = View.VISIBLE
            linearLayout8.visibility = View.GONE
            linearLayout7.visibility = View.GONE
            val ins = applicationContext.contentResolver.openInputStream(data?.data)
            val bitmap = BitmapFactory.decodeStream(ins)
            ins.close()
            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream)
            val byteArray = byteArrayOutputStream.toByteArray()
            //b64.append(gu.encode(byteArray))
            b64.append(Base64.encodeToString(byteArray, Base64.DEFAULT))
            //b64.append(gu.encode(byteArray))
            //Log.i("VIEWDATA", b64.toString())
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun processCapturedPhoto() {
        val cursor = contentResolver.query(
            Uri.parse(mCurrentPhotoPath),
            Array(1) {android.provider.MediaStore.Images.ImageColumns.DATA},
            null, null, null)
        cursor.moveToFirst()
        val photoPath = cursor.getString(0)
        cursor.close()
        var o = BitmapFactory.Options()
        o.inJustDecodeBounds = true
        BitmapFactory.decodeFile(photoPath, o)
        val REQUIRED_SIZE = 450
        var width_tmp = o.outWidth
        var height_tmp = o.outHeight
        var scale = 1
        var exi = true
        //Log.i("VIEWDATA", width_tmp.toString() + ", " + height_tmp.toString())

        while((width_tmp > REQUIRED_SIZE) && (height_tmp > REQUIRED_SIZE)){
            width_tmp /= 2
            height_tmp /= 2
            scale *= 2
        }
        var o2 = BitmapFactory.Options()
        o2.inSampleSize = scale
        var bitmap = BitmapFactory.decodeFile(photoPath, o2)
        imageView29.setImageBitmap(bitmap)
        hasPhoto = true
        frameLayout.visibility = View.VISIBLE
        linearLayout8.visibility = View.GONE
        linearLayout7.visibility = View.GONE

        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        Log.i("VIEWDATA", "byteArray.size.toString(): " + byteArray.size.toString())
        b64.append(Base64.encodeToString(byteArray, Base64.DEFAULT))
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    fun publishMessage() {
        dIP = gu.getIPAddress(true)
        if(dIP.equals("NOT_FOUND"))
            dIP = "0.0.0.0"
        val url = gu.getApiRestURL("makpos")
        val vals: HashMap<String, String> = hashMapOf(
            "v1" to pos,
            "v2" to dname,
            "v3" to fname,
            "v4" to dba.getParamByName("oni"),
            "v5" to "",
            "v6" to tit,
            "v7" to "PRI",
            "v8" to "0",
            "v9" to "AND",
            "v10" to "0",
            "v11" to dIP,
            "v12" to dba.getParamByName("nic")
        )
        request.POST(url, vals, object: Callback {
            override fun onResponse(call: Call?, response: Response) {
                val responseData = response.body()?.string()
                runOnUiThread{
                    try {
                        var json = JSONObject(responseData)

                        var jsdats = json.getJSONArray("dats")
                        var jsdtArrayObject = jsdats.getJSONObject(0)

                        s_res = jsdtArrayObject.getString("res")
                        s_men = jsdtArrayObject.getString("men")

                        if(s_res.equals("1")){
                            var jsdatarray = jsdtArrayObject.getJSONArray("dtarray")
                            var jsdtrow = jsdatarray.getJSONObject(0)

                            postok = jsdtrow.getString("idr")

                            editText5.setText("")
                            editText6.setText("")
                            hasPhoto = false
                            frameLayout.visibility = View.GONE
                            linearLayout8.visibility = View.VISIBLE
                            linearLayout7.visibility = View.VISIBLE
                            b64 = StringBuilder()
                        } else {
                            postok = "FAIL"
                        }
                    } catch (e: JSONException) {
                        postok = "FAIL"
                    }
                    progressBar2.visibility = View.GONE
                    textView40.visibility = View.GONE
                    button4.isEnabled = true
                    finish()
                }
            }

            override fun onFailure(call: Call?, e: IOException?) {
                postok = "FAIL"
                progressBar2.visibility = View.GONE
                textView40.visibility = View.GONE
                button4.isEnabled = true
            }
        })
    }

    fun sendImageToDocsServer(){
        //Log.i("VIEWDATA", "sendImageToDocsServer()")
        dIP = gu.getIPAddress(true)
        if(dIP.equals("NOT_FOUND"))
            dIP = "0.0.0.0"
        dMac = gu.getMACAddress("wlan0")
        //Log.i("VIEWDATA", dIP)
        val url = gu.getApiRestURL("savpic")
        val vals: HashMap<String, String> = hashMapOf(
            "v1" to "data:image/jpeg;base64," + b64.toString(),
            "v2" to "PNG",
            "v3" to "4",
            "v4" to dIP,
            "v5" to dba.getParamByName("dui"),
            "v6" to "AND_APP"
        )
        request.POST(url, vals, object: Callback {
            override fun onResponse(call: Call?, response: Response) {
                val responseData = response.body()?.string()
                runOnUiThread{
                    try {
                        //Log.i("VIEWDATA", responseData)
                        var json = JSONObject(responseData)

                        var jsdats = json.getJSONArray("dats")
                        var jsdtArrayObject = jsdats.getJSONObject(0)

                        s_res = jsdtArrayObject.getString("res")
                        s_men = jsdtArrayObject.getString("men")

                        if(s_res.equals("1")){
                            var jsdatarray = jsdtArrayObject.getJSONArray("dtarray")
                            var jsdtrow = jsdatarray.getJSONObject(0)

                            fname = jsdtrow.getString("fna")

                            //Log.i("VIEWDATA", "fna: " + fname)
                            publishMessage()
                        } else {
                            fname = "SINIMAGEN"
                        }
                    } catch (e: JSONException) {
                        fname = "SINIMAGEN"
                        //Log.i("VIEWDATA", e.message)
                    }
                }
            }

            override fun onFailure(call: Call?, e: IOException?) {
                fname = "SINIMAGEN"
                Log.i("VIEWDATA",  e.toString())
                //Log.i("VIEWDATA", "FAIL4")
            }
        })
    }
}