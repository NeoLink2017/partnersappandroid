package com.gintelsys.partners.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.gintelsys.partners.HomeActivity
import com.gintelsys.partners.R
import com.gintelsys.partners.data.dbHandler
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.gintelsys.partners.utils.GeneralUtils



class globalFirebaseMessagingService : FirebaseMessagingService() {
    val TAG = "Service"
    lateinit var gu : GeneralUtils
    var requestID : Int = System.currentTimeMillis().toInt()
    lateinit var dba : dbHandler

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        var data : Map<String, String> = remoteMessage?.data!!
        dba = dbHandler(this, null, null, 1)
        gu = GeneralUtils(this)

        var men : String = ""
        val usu : String = data.get("usu").toString()
        val tip : String = data.get("tip").toString()
        val rea : String = data.get("rea").toString()
        val tar : String = data.get("tar").toString()

        if(dba.hasAccount()){
            val dbusu : String = dba.getParamByName("nic")

            //Log.i("VIEWDATA", "usu: " + usu + "|dbusu: " + dbusu)

            if(tip.equals("NEWPOS")){
                if(!(usu.equals(dbusu))){
                    val tex  =  gu.getRamdom(1, 5)
                    when (tex){
                        1 -> men = usu + " Ha publicado algo que puede interesarte"
                        2 -> men = usu + " Acaba de publicar algo"
                        3 -> men = usu + " Realizo una publicacion"
                        4 -> men = usu + " Ha dicho esto..."
                        5 -> men = usu + " Escribio..."
                        else -> men = usu + " Dijo esto, que piensas tu?"
                    }

                    sendNotification(men, "Nueva Publicacion en Partners!")
                }
            } else if(tip.equals("COMMEN")){
                if(tar.equals(dbusu)){
                    men = usu + " ha comentado tu publicacion"
                    sendNotification(men, "Recibiste un comentario!")
                }

            } else if(tip.equals("POSREA")){
                if(tar.equals(dbusu)){
                    val opt = rea.toInt()
                    when (opt){
                        1 -> men = "A " + usu + " le gusto tu publicacion"
                        2 -> men = "A " + usu + " le encanto tu publicacion"
                        3 -> men = "A " + usu + " le asombro tu publicacion"
                        4 -> men = "A " + usu + " le molesto tu publicacion"
                        5 -> men = "A " + usu + " le entristece tu publicacion"
                        6 -> men = "A " + usu + " le divierte tu publicacion"
                        else -> men = usu + " reacciono a tu publicacion"
                    }

                    sendNotification(men, "Alguien reacciono a tu Post!")
                }
            } else if(tip.equals("DELPOS")){
                dba.deleteLocalPost(tar)
            } else if(tip.equals("REMACO")){
                val dui = dba.getParamByName("dui")
                if(tar.equals(dui)){
                    dba.removeAccount()
                }
            }
        }
    }

    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)
    }

    private fun sendNotification(message : String, titulo: String) {
        val intent = Intent(this, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
            PendingIntent.FLAG_ONE_SHOT)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
            .setContentText(message)
            .setAutoCancel(true)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val channelId : String =  this.getString(R.string.default_notification_channel_id)
            val channel : NotificationChannel = NotificationChannel(channelId, titulo, NotificationManager.IMPORTANCE_DEFAULT)
            channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            channel.description = message
            notificationManager.createNotificationChannel(channel)
            notificationBuilder.setChannelId(channelId)
        }

        notificationManager.notify(requestID, notificationBuilder.build())
    }

    private fun sendRegistrationIDToDB(rid : String){
        gu = GeneralUtils(applicationContext)

        gu.sendFBID(rid)
    }
}