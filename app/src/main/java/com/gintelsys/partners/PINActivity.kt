package com.gintelsys.partners

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.gintelsys.partners.data.dbHandler
import kotlinx.android.synthetic.main.activity_pin.*

class PINActivity  : AppCompatActivity() {

    val dba = dbHandler(this, null, null, 1)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pin)

        button5.setOnClickListener(View.OnClickListener {
            textView58.visibility = View.GONE
            val pin = editText7.text.toString().trim()

            if(pin.length == 4){
                val dui = dba.getParamByName("dui")
                if(dui.length >= 4){
                    if(pin.equals(dui.substring(0, 4))){
                        goToHome()
                    } else {
                        textView58.visibility = View.VISIBLE
                        textView58.text = "PIN INCORRECTO, INTENTA DE NUEVO"
                    }
                }
            } else {
                textView58.visibility = View.VISIBLE
                textView58.text = "TU PIN DEBE SER DE 4 DIGITOS"
            }
        })
    }

    fun goToHome() {
        val i = Intent(applicationContext, HomeActivity::class.java)
        startActivity(i)
    }
}