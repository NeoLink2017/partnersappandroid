package com.gintelsys.partners

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.telephony.TelephonyManager
import android.text.InputFilter
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_login.*
import com.gintelsys.partners.utils.GeneralUtils
import com.gintelsys.partners.data.dbHandler
import okhttp3.OkHttpClient
import com.gintelsys.partners.network.OkHttpRequest
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.lang.Exception

class LoginActivity : AppCompatActivity() {
    var gu = GeneralUtils(this)
    val dba = dbHandler(this, null, null, 1)
    var client = OkHttpClient()
    var request = OkHttpRequest(client)
    var s_res = ""
    var s_men = ""
    var usu = ""
    var dui = ""
    var pas = ""
    var IMEI = ""
    var dMar = ""
    var dMod = ""
    var dIP = ""
    var dMac = ""
    var dOS = ""
    var dNum = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        editText2.setFilters(arrayOf<InputFilter>(InputFilter.AllCaps()))

        gu.setRestServer("production")
        setLayout()

        button.setOnClickListener(View.OnClickListener {
            setLayout()

            usu = editText.text.toString().trim()
            pas = editText2.text.toString().trim()
            var allok = 0
            //Log.i("VIEWDATA", "clickec")
            if(usu.length == 0){
                imageView4.visibility = View.VISIBLE
                textView8.visibility = View.VISIBLE
                textView8.text = "DIGITA TU USUARIO - DUI"
                allok += 1
            } else {
                if(usu.length == 9){
                    dui = usu.substring(0, 8) + "-" + usu.substring(8, 9);
                } else {
                    imageView4.visibility = View.VISIBLE
                    textView8.visibility = View.VISIBLE
                    textView8.text = "DUI DEBE SER DE 9 DIGITOS"
                    allok += 1
                }
            }
            if(pas.length == 0){
                imageView6.visibility = View.VISIBLE
                textView11.visibility = View.VISIBLE
                textView11.text = "DIGITA TU CLAVE"
                allok += 1
            }

            if(allok == 0){
                //Log.i("VIEWDATA", "doing")
                if(gu.isNetworkAvailable){
                    doXpressLogin()
                } else {
                    gu.displayAlert("SIN ACCESO A INTERNET", "Partners App requiere acceso a Internet. Verifica tu conexion o revisa si esta App tiene Permisos para Internet", applicationContext)
                }
            } else {
                textView12.visibility = View.VISIBLE
                textView12.text = "VERIFICA LOS DATOS INGRESADOS"
            }
        })
    }

    fun doXpressLogin() {
        dIP = gu.getIPAddress(true)
        dMac = gu.getMACAddress("wlan0")

        val url = gu.getApiRestURL("mlog")
        //Log.i("VIEWDATA", url)
        val vals: HashMap<String, String> = hashMapOf(
            "v1" to usu,
            "v2" to pas,
            "v3" to getDeviceImei(),
            "v4" to dMar,
            "v5" to dMod,
            "v6" to dIP,
            "v7" to dMac,
            "v8" to dOS,
            "v9" to dNum
        )

        request.POST(url, vals, object: Callback {
            override fun onResponse(call: Call?, response: Response) {
                val responseData = response.body()?.string()
                runOnUiThread{
                    try {
                        var json = JSONObject(responseData)
                        var jsdats = json.getJSONArray("dats")
                        var jsdtArrayObject = jsdats.getJSONObject(0)

                        s_res = jsdtArrayObject.getString("res")
                        s_men = jsdtArrayObject.getString("men")

                        if(s_res.equals("1")){
                            var jsdatarray = jsdtArrayObject.getJSONArray("dtarray")
                            var jsdtrow = jsdatarray.getJSONObject(0)

                            var k_res = jsdtrow.getString("res")
                            var k_men = jsdtrow.getString("men")

                            if(k_res.equals("1")){
                                var k_sid = jsdtrow.getString("sid")
                                var k_oni = jsdtrow.getString("oni")
                                var k_nom = jsdtrow.getString("nom")
                                var k_est = jsdtrow.getString("est")
                                var k_nic = jsdtrow.getString("nic")

                                dba.addCuentaItem("sid", k_sid)
                                dba.addCuentaItem("oni", k_oni)
                                dba.addCuentaItem("nom", k_nom)
                                dba.addCuentaItem("nic", k_nic)
                                dba.addCuentaItem("est", k_est)
                                dba.addCuentaItem("unidevid", getDeviceImei())
                                dba.addCuentaItem("dui", dui)
                                dba.addCuentaItem("fbid", "")
                                goToHome()
                            } else {
                                textView12.text = "DATOS INCORRECTOS"
                            }
                        } else {
                            textView12.text = "OCURRIO UN PROBLEMA, INTENTA DE NUEVO POR FAVOR."
                        }
                    } catch (e: JSONException) {
                        textView12.text = "OCURRIO UN PROBLEMA, INTENTA DE NUEVO POR FAVOR."
                    }
                }
            }

            override fun onFailure(call: Call?, e: IOException?) {
                //Log.i("VIEWDATA",  e.toString())
                //Log.i("VIEWDATA", "FAIL4")
            }
        })
    }

    fun setLayout() {
        imageView4.visibility = View.GONE
        imageView6.visibility = View.GONE
        textView8.visibility = View.INVISIBLE
        textView11.visibility = View.INVISIBLE
        textView12.visibility = View.INVISIBLE
    }

    fun getDeviceImei(): String {
        try{
            val tm = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_PHONE_STATE)) {
                    IMEI = tm.getDeviceId()
                    dNum = tm.line1Number
                    dMar = Build.MANUFACTURER
                    dMod = Build.MODEL
                    dOS = Build.VERSION.RELEASE

                } else {
                    IMEI = "NOT_FOUND"
                    dNum = "NOT_FOUND"
                    dMar = Build.MANUFACTURER
                    dMod = Build.MODEL
                    dOS = Build.VERSION.RELEASE
                    return IMEI
                }
            }

            if (IMEI != null){
                IMEI = Settings.Secure.getString(applicationContext.contentResolver, Settings.Secure.ANDROID_ID)
                dNum = "00000000"
                dMar = Build.MANUFACTURER
                dMod = Build.MODEL
                dOS = Build.VERSION.RELEASE
                return IMEI
            } else {
                dNum = "00000000"
                IMEI = "NOT_FOUND"
                dMar = Build.MANUFACTURER
                dMod = Build.MODEL
                dOS = Build.VERSION.RELEASE
                return IMEI
            }
        } catch (ex: Exception){
            dNum = "00000000"
            IMEI = "NOT_FOUND"
            dMar = Build.MANUFACTURER
            dMod = Build.MODEL
            dOS = Build.VERSION.RELEASE
            return IMEI
        }
    }

    fun goToHome() {
        val i = Intent(applicationContext, HomeActivity::class.java)
        startActivity(i)
    }
}